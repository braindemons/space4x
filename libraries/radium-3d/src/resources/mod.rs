use {
    slotmap::{new_key_type, SlotMap, Key},
    nalgebra::{Point2, Point3, Vector3},
    serde_derive::{Serialize, Deserialize},
};

new_key_type! {
    pub struct PipelineId;
    pub struct MeshId;
    pub struct TextureId;
    pub struct MaterialId;
}

pub struct GraphicsResources<K: Key, D> {
    // We only have to track existence, the data is stored in a backend secondary map
    resources: SlotMap<K, ()>,
    queue: Vec<(K, D)>,
}

#[allow(clippy::new_without_default_derive)]
impl<K: Key + Copy, D> GraphicsResources<K, D> {
    pub fn new() -> Self {
        GraphicsResources {
            resources: SlotMap::with_key(),
            queue: Vec::new(),
        }
    }

    pub fn create(&mut self, data: D) -> K {
        let key = self.resources.insert(());
        self.queue.push((key, data));
        key
    }

    pub fn drain_queue(&mut self) -> std::vec::Drain<(K, D)> {
        self.queue.drain(..)
    }
}

#[derive(Serialize, Deserialize)]
pub struct PipelineData {
    pub vert: Vec<u32>,
    pub frag: Vec<u32>,
}

pub type PipelineResources = GraphicsResources<PipelineId, PipelineData>;

#[repr(C)]
#[derive(Clone, Copy, Serialize, Deserialize)]
pub struct Vertex {
    pub position: Point3<f32>,
    pub normal: Vector3<f32>,
    pub uv: Point2<f32>,
}

/// Data to create a mesh resource.
#[derive(Serialize, Deserialize)]
pub struct MeshData {
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u16>,
}

pub type MeshResources = GraphicsResources<MeshId, MeshData>;

/// Data to create a texture resource.
#[derive(Serialize, Deserialize)]
pub struct TextureData {
    pub width: u32,
    pub height: u32,
    pub blocks: Vec<u8>,
}

pub type TextureResources = GraphicsResources<TextureId, TextureData>;

/// Data to create a material resource.
pub struct MaterialData {
    pub base_color: TextureId,
}

pub type MaterialResources = GraphicsResources<MaterialId, MaterialData>;
