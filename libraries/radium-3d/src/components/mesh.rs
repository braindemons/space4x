use {
    specs::{VecStorage, Component},
    
    crate::resources::{PipelineId, MeshId, MaterialId},
};

/// Attaches a mesh to an entity.
pub struct MeshComponent {
    pub mesh: MeshId,
    pub material: MaterialId,
    pub pipeline: PipelineId,
}

impl Component for MeshComponent {
    type Storage = VecStorage<Self>;
}
