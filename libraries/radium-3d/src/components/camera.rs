use {
    specs::{
        VecStorage, Component,
    },
};

/// Attaches a camera to an entity.
pub struct CameraComponent;

impl Component for CameraComponent {
    type Storage = VecStorage<Self>;
}
