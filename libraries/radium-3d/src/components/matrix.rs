use {
    nalgebra::{Matrix4, one},
    specs::{
        DenseVecStorage, FlaggedStorage, Component,
    },
};

/// Tracks the cascading matrices for the rendering heirarchy.
pub struct MatrixComponent {
    pub matrix: Matrix4<f32>,
}

impl MatrixComponent {
    pub fn new() -> Self {
        MatrixComponent {
            matrix: one(),
        }
    }
}

impl Default for MatrixComponent {
    fn default() -> Self {
        Self::new()
    }
}

impl Component for MatrixComponent {
    type Storage = FlaggedStorage<Self, DenseVecStorage<Self>>;
}
