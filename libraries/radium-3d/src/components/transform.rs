use {
    nalgebra::{Point3, Vector3, UnitQuaternion, Matrix4, one},
    specs::{
        DenseVecStorage, FlaggedStorage, Component, Entity,
    },
    specs_hierarchy::{Parent},
};

/// Attaches a 3D world transform to an entity.
pub struct TransformComponent {
    pub position: Point3<f32>,
    pub scale: Vector3<f32>,
    pub orientation: UnitQuaternion<f32>,
}

impl TransformComponent {
    pub fn at(position: Point3<f32>) -> Self {
        Self {
            position,
            scale: Vector3::new(1.0, 1.0, 1.0),
            orientation: one(),
        }
    }

    pub fn with_scale(mut self, scale: Vector3<f32>) -> Self {
        self.scale = scale;
        self
    }

    pub fn with_scale_uniform(mut self, scale: f32) -> Self {
        self.scale = Vector3::new(scale, scale, scale);
        self
    }

    pub fn with_orientation(mut self, orientation: UnitQuaternion<f32>) -> Self {
        self.orientation = orientation;
        self
    }

    pub fn matrix(&self) -> Matrix4<f32> {
        let translation = Matrix4::new_translation(&self.position.coords);
        let scale = Matrix4::new_nonuniform_scaling(&self.scale);
        let orientation_matrix = self.orientation.to_homogeneous();
        translation * scale * orientation_matrix
    }
}

impl Component for TransformComponent {
    type Storage = FlaggedStorage<Self, DenseVecStorage<Self>>;
}

/// Parents an entity's `TransformComponent` to another one.
pub struct ParentComponent {
    pub parent: Entity,
}

impl Component for ParentComponent {
    type Storage = FlaggedStorage<Self, DenseVecStorage<Self>>;
}

impl Parent for ParentComponent {
    fn parent_entity(&self) -> Entity {
        self.parent
    }
}
