use {
    serde_derive::{Serialize, Deserialize},

    crate::resources::{PipelineData, MeshData, TextureData},
};

#[derive(Serialize, Deserialize)]
pub struct MeshAsset {
    pub data: MeshData,
}

#[derive(Serialize, Deserialize)]
pub struct TextureAsset {
    pub data: TextureData,
}

#[derive(Serialize, Deserialize)]
pub struct PipelineAsset {
    pub data: PipelineData,
}
