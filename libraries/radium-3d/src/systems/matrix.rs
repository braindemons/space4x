use {
    specs::{
        storage::{ComponentEvent},
        BitSet, System, WriteStorage, Entities, ReaderId, SystemData, Resources, ReadStorage,
        ReadExpect, Join, Entity,
    },
    specs_hierarchy::{HierarchyEvent},

    crate::{
        components::{ParentComponent, TransformComponent, MatrixComponent},
        systems::{ParentHierarchy},
    },
};

/// Updates all `MatrixComponent`s based on updates to `TransformComponent`s or `ParentComponent`s.
#[derive(Default)]
pub struct UpdateMatricesSystem {
    transform_modified: BitSet,
    matrix_modified: BitSet,

    component_id: Option<ReaderId<ComponentEvent>>,
    hierarchy_id: Option<ReaderId<HierarchyEvent>>,

    scratch: Vec<Entity>,
}

impl UpdateMatricesSystem {
    pub fn new() -> Self {
        UpdateMatricesSystem {
            transform_modified: BitSet::default(),
            matrix_modified: BitSet::default(),

            component_id: None,
            hierarchy_id: None,

            scratch: Vec::new(),
        }
    }

    /// Find all entities that have a transform component, but no matrix component, and add matrix
    /// components for them, avoiding accidentally not adding those.
    fn add_matrix_components<'a>(
        &mut self,
        entities: &Entities<'a>,
        transforms: &ReadStorage<'a, TransformComponent>,
        matrices: &mut WriteStorage<'a, MatrixComponent>,
    ) {
        self.scratch.clear();
        self.scratch.extend((&**entities, transforms, !&*matrices).join().map(|d| d.0));
        for entity in &self.scratch {
            matrices.insert(*entity, MatrixComponent::default()).unwrap();
        }
    }

    fn mark_changed<'a>(
        &mut self,
        entities: &Entities<'a>,
        hierarchy: &ReadExpect<'a, ParentHierarchy>,
        transforms: &ReadStorage<'a, TransformComponent>,
    ) {
        self.transform_modified.clear();

        // We need to find all the entities that need to have their matrix updated, so start by
        // getting all the new added and modified components
        let events = transforms
            .channel()
            .read(self.component_id.as_mut().unwrap());
        for event in events {
            match event {
                ComponentEvent::Modified(id) => {
                    self.transform_modified.add(*id);
                }
                ComponentEvent::Inserted(id) => {
                    self.transform_modified.add(*id);
                }
                _ => {}
            }
        }

        // Check if any changes to the scene hierarchy happened as well
        for event in hierarchy
            .changed()
            .read(self.hierarchy_id.as_mut().unwrap())
        {
            match *event {
                HierarchyEvent::Removed(entity) => {
                    // Sometimes the user may have already deleted the entity.
                    // This is fine, so we'll ignore any errors this may give
                    // since it can only fail due to the entity already being dead.
                    let _ = entities.delete(entity);
                }
                HierarchyEvent::Modified(entity) => {
                    self.transform_modified.add(entity.id());
                }
            }
        }
    }

    fn update_matrices<'a>(
        &mut self,
        entities: &Entities<'a>,
        hierarchy: &ReadExpect<'a, ParentHierarchy>,
        transforms: &ReadStorage<'a, TransformComponent>,
        parents: &ReadStorage<'a, ParentComponent>,
        matrices: &mut WriteStorage<'a, MatrixComponent>,
    ) {
        self.matrix_modified.clear();
        
        // Update all changed parentless entities
        for (entity, _, transform, matrix, ()) in (
            &**entities,
            &self.transform_modified, // Only modified entities
            transforms,
            &mut *matrices,
            !parents, // Eliminate entities with parents
        ).join()
        {
            self.matrix_modified.add(entity.id());
            matrix.matrix = transform.matrix();
        }

        // Update all components with parents, all() makes sure we're iterating in the order needed
        // to cascade transform changes down as we need to
        for entity in hierarchy.all() {
            // Check if this entity needs updating itself
            let self_dirty = self.transform_modified.contains(entity.id());

            // If we have both a parent and a transform component
            if let (Some(parent), Some(transform)) =
                (parents.get(*entity), transforms.get(*entity))
            {
                // Check if our parent was updated
                let parent_dirty = self.matrix_modified.contains(parent.parent.id());
                
                // If either the parent was updated or this entity's transform was changed, we need
                // to update its matrix, cascading down changes
                if parent_dirty || self_dirty {
                    // If the parent has a matrix, use that, if not just use the local matrix
                    let combined_transform =
                        if let Some(parent_matrix) = matrices.get(parent.parent) {
                            parent_matrix.matrix * transform.matrix()
                        } else {
                            transform.matrix()
                        };

                    // Now that we finally have a matrix, assign it to our current component
                    if let Some(matrix) = matrices.get_mut(*entity) {
                        self.matrix_modified.add(entity.id());
                        matrix.matrix = combined_transform;
                    }
                }
            }
        }
    }
}

impl<'a> System<'a> for UpdateMatricesSystem {
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, ParentHierarchy>,
        ReadStorage<'a, TransformComponent>,
        ReadStorage<'a, ParentComponent>,
        WriteStorage<'a, MatrixComponent>,
    );

    fn run(&mut self, (entities, hierarchy, transforms, parents, mut matrices): Self::SystemData) {
        self.add_matrix_components(&entities, &transforms, &mut matrices);

        self.mark_changed(&entities, &hierarchy, &transforms);

        self.update_matrices(&entities, &hierarchy, &transforms, &parents, &mut matrices);
    }

    fn setup(&mut self, res: &mut Resources) {
        Self::SystemData::setup(res);
        
        let mut transforms: WriteStorage<TransformComponent> = SystemData::fetch(&res);
        self.component_id = Some(transforms.register_reader());

        let mut hierarchy = res.fetch_mut::<ParentHierarchy>();
        self.hierarchy_id = Some(hierarchy.track());
    }
}