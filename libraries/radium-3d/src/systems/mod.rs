mod matrix;

pub use self::{
    matrix::{UpdateMatricesSystem},
};

pub type ParentHierarchy =
    specs_hierarchy::Hierarchy<crate::components::ParentComponent>;

pub type ParentHierarchySystem =
    specs_hierarchy::HierarchySystem<crate::components::ParentComponent>;
