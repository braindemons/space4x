use {
    std::{
        collections::{HashMap},
        path::{PathBuf},
    },

    image::{SubImage},

    radium_assets::{build::{AssetBuilder, Error}},
    radium_3d::{
        assets::{TextureAsset},
        resources::{TextureData},
    },
};

pub struct TextureAssetBuilder;

impl AssetBuilder for TextureAssetBuilder {
    type AssetData = TextureAsset;

    fn build(
        &self,
        data: &HashMap<String, String>,
        source_directory: &PathBuf,
        _temp_directory: &PathBuf,
    ) -> Result<TextureAsset, Error> {
        let mut blend_file = source_directory.clone();
        blend_file.push(&data["source"]);

        let asset = image_to_asset(&blend_file);

        Ok(asset)
    }
}

pub fn image_to_asset(source: &PathBuf) -> TextureAsset {
    let mut image = image::open(source).unwrap().to_rgba();
    let (width, height) = image.dimensions();
    let (blocks_width, blocks_height) = (width / 4, height / 4);

    let mut data =TextureData {
        width,
        height,
        blocks: vec!(0; (8*blocks_width*blocks_height) as usize),
    };

    for block_y in 0..blocks_height {
        for block_x in 0..blocks_width {
            let source = SubImage::new(&mut image, block_x * 4, block_y * 4, 4, 4)
                .to_image();

            let index = ((block_y * blocks_width + block_x) * 8) as usize;
            let data_slice = &mut data.blocks[index..index+8];

            unsafe {
                stb_dxt::stb_compress_dxt_block(
                    data_slice.as_mut_ptr(),
                    source.as_ptr(),
                    0,
                    stb_dxt::STB_DXT_NORMAL,
                );
            }
        }
    }

    TextureAsset { data }
}
