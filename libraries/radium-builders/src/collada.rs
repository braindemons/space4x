use {
    std::{io::{Read}},

    serde_derive::{Deserialize},
    serde_xml_rs::{deserialize as xml_deserialize},
};

#[derive(Deserialize, Debug)]
pub struct Collada {
    pub asset: Option<Asset>,
    #[serde(rename = "library_geometries", default)]
    pub library_geometries: Vec<LibraryGeometries>,
}

#[derive(Deserialize, Debug)]
pub struct Asset {
    #[serde(rename = "contributor", default)]
    pub contributors: Vec<Contributor>,
    pub created: String,
    pub keywords: Option<String>,
    pub modified: String,
    pub revision: Option<String>,
    pub subject: Option<String>,
    pub title: Option<String>,
    pub unit: Option<Unit>,
    pub up_axis: Option<String>,
}

#[derive(Deserialize, Debug)]
pub struct Contributor {
    pub author: String,
    pub authoring_tool: String,
}

#[derive(Deserialize, Debug)]
pub struct Unit {
    pub name: String,
    pub meter: String,
}

#[derive(Deserialize, Debug)]
pub struct LibraryGeometries {
    pub asset: Option<Asset>,
    #[serde(rename = "geometry", default)]
    pub geometries: Vec<Geometry>,
}

#[derive(Deserialize, Debug)]
pub struct Geometry {
    pub id: String,
    pub name: String,
    pub asset: Option<Asset>,
    pub mesh: Option<Mesh>,
}

#[derive(Deserialize, Debug)]
pub struct Mesh {
    #[serde(rename = "source", default)]
    pub sources: Vec<Source>,
    pub vertices: Vertices,
    #[serde(rename = "polylist", default)]
    pub polylists: Vec<Polylist>,
    #[serde(rename = "triangles", default)]
    pub triangles: Vec<Triangles>,
}

#[derive(Deserialize, Debug)]
pub struct Source {
    pub id: String,
    pub name: Option<String>,
    pub asset: Option<Asset>,
    pub float_array: Option<FloatArray>,
}

#[derive(Deserialize, Debug)]
pub struct FloatArray {
    pub count: String,
    pub id: Option<String>,
    pub name: Option<String>,
    pub digits: Option<String>,
    pub magnitude: Option<String>,
    #[serde(rename = "$value")]
    pub content: String,
}

#[derive(Deserialize, Debug)]
pub struct Vertices {
}

#[derive(Deserialize, Debug)]
pub struct Polylist {
    #[serde(rename = "input", default)]
    pub inputs: Vec<Input>,
    pub vcount: Option<String>,
    pub p: Option<String>,
}

#[derive(Deserialize, Debug)]
pub struct Triangles {
    pub count: String,
    pub material: String,
    #[serde(rename = "input", default)]
    pub input: Vec<Input>,
    pub p: Option<String>,
}

#[derive(Deserialize, Debug)]
pub struct Input {
    pub offset: String,
    pub semantic: String,
    pub source: String,
    pub set: Option<String>,
}

pub fn deserialize<R: Read>(reader: R) -> Collada {
    xml_deserialize(reader).unwrap()
}
