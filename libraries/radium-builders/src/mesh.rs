use {
    std::{
        collections::{HashMap},
        path::{PathBuf},
        process::{Command},
        fs::{File},
    },

    nalgebra::{Matrix4, Point2, Point3, Vector3},

    radium_assets::{
        build::{AssetBuilder, Error},
    },
    radium_3d::{
        assets::{MeshAsset},
        resources::{Vertex, MeshData},
    },
};

pub struct MeshAssetBuilder {
    pub blender: String,
}

impl AssetBuilder for MeshAssetBuilder {
    type AssetData = MeshAsset;

    fn build(
        &self,
        data: &HashMap<String, String>,
        source_directory: &PathBuf,
        temp_directory: &PathBuf,
    ) -> Result<MeshAsset, Error> {
        let mut blend_file = source_directory.clone();
        blend_file.push(&data["source"]);

        let mut dae_file = temp_directory.clone();
        dae_file.push(blend_file.file_name().unwrap());
        dae_file.set_extension("dae");

        let output = Command::new(&self.blender)
            .arg(&blend_file)
            .arg("--background")
            .arg("--python").arg("./export.py")
            .env("AT_DAE_TARGET", &dae_file)
            .output()
            .unwrap();
        if !output.status.success() {
            return Err(format!(
                "Failed to export COLLADA:\n{}",
                String::from_utf8_lossy(&output.stdout),
            ).into())
        }

        let asset = collada_to_mesh(&dae_file);

        Ok(asset)
    }
}

fn collada_to_mesh(dae_file: &PathBuf) -> MeshAsset {
    let file = File::open(dae_file).unwrap();
    let document = crate::collada::deserialize(file);
    let mesh = document.library_geometries[0].geometries[0].mesh.as_ref().unwrap();

    let matrix = Matrix4::from_euler_angles(std::f32::consts::PI * 2.0 * -0.25, 0.0, 0.0);

    let positions: Vec<f32> = mesh.sources[0].float_array.as_ref().unwrap().content
        .split(' ')
        .map(|p| p.parse().unwrap())
        .collect();
    let positions: Vec<_> = positions.chunks(3)
        // Convert from z-up to y-up
        .map(|value| matrix.transform_point(&Point3::new(value[0], value[1], value[2])))
        .collect();

    let normals: Vec<f32> = mesh.sources[1].float_array.as_ref().unwrap().content
        .split(' ')
        .map(|p| p.parse().unwrap())
        .collect();
    let normals: Vec<_> = normals.chunks(3)
        // Convert from z-up to y-up
        .map(|value| matrix.transform_vector(&Vector3::new(value[0], value[1], value[2])))
        .collect();

    let uvs: Vec<f32> = mesh.sources[2].float_array.as_ref().unwrap().content
        .split(' ')
        .map(|p| p.parse().unwrap())
        .collect();
    let uvs: Vec<_> = uvs.chunks(2)
        // Convert from OpenGL UVs to Vulkan UVs
        .map(|value| Point2::new(value[0], 1.0 - value[1]))
        .collect();

    let mut vertices: Vec<Vertex> = Vec::new();
    let mut indices: Vec<u16> = Vec::new();

    let vcounts: Vec<usize> = mesh.polylists[0].vcount.as_ref().unwrap()
        .split(' ')
        .map(|v| v.parse().unwrap())
        .collect();
    for v in vcounts {
        assert_eq!(v, 3, "Mesh is not triangulated");
    }

    let collada_indices: Vec<usize> = mesh.polylists[0].p.as_ref().unwrap()
        .split(' ')
        .map(|p| p.parse().unwrap())
        .collect();
    for cindices in collada_indices.chunks(3) {
        vertices.push(Vertex {
            position: positions[cindices[0]],
            normal: normals[cindices[1]],
            uv: uvs[cindices[2]],
        });
        // TODO: Eliminate duplicates to minimize mesh size
        indices.push((vertices.len()-1) as u16);
    }

    MeshAsset {
        data: MeshData {
            vertices,
            indices,
        }
    }
}