use {
    std::{
        collections::{HashMap},
        io::{Read},
        path::{PathBuf},
        process::{Command},
        fs::{File},
    },

    byteorder::{LittleEndian, ByteOrder},

    radium_assets::{build::{AssetBuilder, Error}},
    radium_3d::{
        assets::{PipelineAsset},
        resources::{PipelineData},
    },
};

pub struct PipelineAssetBuilder;

impl AssetBuilder for PipelineAssetBuilder {
    type AssetData = PipelineAsset;

    fn build(
        &self,
        data: &HashMap<String, String>,
        source_directory: &PathBuf,
        temp_directory: &PathBuf,
    ) -> Result<PipelineAsset, Error> {
        let mut vert_file = source_directory.clone();
        vert_file.push(&data["vert"]);
        let mut frag_file = source_directory.clone();
        frag_file.push(&data["frag"]);

        let vert = build_spirv(&vert_file, temp_directory)?;
        let frag = build_spirv(&frag_file, temp_directory)?;

        let asset = PipelineAsset {
            data: PipelineData { vert, frag, },
        };

        Ok(asset)
    }
}

fn build_spirv(
    source_file: &PathBuf, temp_directory: &PathBuf,
) -> Result<Vec<u32>, Error> {
    let mut target_file = temp_directory.clone();
    target_file.push(source_file.file_name().unwrap());
    target_file.set_extension("spv");

    let output = Command::new("glslc")
        .arg("-o").arg(&target_file)
        .arg(&source_file)
        .output()
        .unwrap();

    if !output.status.success() {
        return Err(format!(
            "Failed to compile GLSL:\n{}",
            String::from_utf8_lossy(&output.stderr)
        ).into())
    }

    // Load in the created temp file as spirv
    let mut reader = File::open(&target_file)?;
    let mut spirv = Vec::new();
    let mut data = [0u8; 4];
    loop {
        let len = reader.read(&mut data)?;

        match len {
            0 => break,
            4 => {},
            _ => panic!("Asset is not a valid SPIR-V module"),
        }

        spirv.push(LittleEndian::read_u32(&data));
    }

    Ok(spirv)
}