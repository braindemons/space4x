mod logging;

pub use self::{
    logging::{setup_logging},
};

use {
    specs::{Resources},
};

pub trait Window {
    fn close_requested(&self) -> bool;

    fn poll_events(&mut self);
}

pub trait Renderer {
    /// Sets up the components and resources needed to render in a world.
    fn setup(&mut self, res: &mut Resources);

    fn render(&mut self, res: &Resources);
}