use {
    std::fmt::{Write},

    log::{Level, LevelFilter},
    yansi::{Paint},
};

pub fn setup_logging() {
    fern::Dispatch::new()
        .format(move |out, message, record| {
            match record.level() {
                Level::Info =>
                    out.finish(format_args!(
                        "{} {}",
                        Paint::blue("INFO"),
                        message,
                    )),
                Level::Trace =>
                    out.finish(format_args!(
                        "{} {}",
                        Paint::purple("TRAC"),
                        message,
                    )),
                Level::Error =>
                    out.finish(format_args!(
                        "{} {}",
                        Paint::red("ERRO").bold(),
                        message,
                    )),
                Level::Warn =>
                    out.finish(format_args!(
                        "{} {}",
                        Paint::yellow("WARN").bold(),
                        message,
                    )),
                Level::Debug => {
                    let mut file_line = String::new();
                    if let Some(file) = record.file() {
                        write!(file_line, "{}", Paint::blue(file)).unwrap();
                    }
                    if let Some(line) = record.line() {
                        write!(file_line, ":{}", Paint::blue(line)).unwrap();
                    }

                    out.finish(format_args!(
                        "{} {}\n{}",
                        Paint::blue("-->").bold(),
                        file_line,
                        record.args(),
                    ));
                }
            }
        })
        .level_for("serde_xml_rs", LevelFilter::Info)
        .level(LevelFilter::Debug)
        .chain(std::io::stdout())
        .apply()
        .unwrap();
}
