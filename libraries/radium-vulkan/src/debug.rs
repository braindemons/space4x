use {
    std::{
        ffi::{CStr},
        ptr::{null, null_mut},
    },
    
    ash::{
        extensions::{DebugReport},
        vk, Entry, Instance,
    },
    log::{info, error},
};

pub fn create_callback(
    entry: &Entry, instance: &Instance,
) -> (DebugReport, vk::DebugReportCallbackEXT) {
    info!("Creating Debug Report Callback");

    let debug_info = vk::DebugReportCallbackCreateInfoEXT {
        s_type: vk::StructureType::DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT,
        p_next: null(),
        flags: vk::DebugReportFlagsEXT::ERROR | vk::DebugReportFlagsEXT::WARNING |
            vk::DebugReportFlagsEXT::PERFORMANCE_WARNING,
        pfn_callback: Some(debug_callback),
        p_user_data: null_mut(),
    };

    let debug_report_ext = DebugReport::new(entry, instance);
    let callback = unsafe { debug_report_ext.create_debug_report_callback_ext(
        &debug_info, None,
    ) }.unwrap();

    (debug_report_ext, callback)
}

unsafe extern "system" fn debug_callback(
    _: vk::DebugReportFlagsEXT,
    _: vk::DebugReportObjectTypeEXT,
    _: u64,
    _: usize,
    _: i32,
    _: *const std::os::raw::c_char,
    p_message: *const std::os::raw::c_char,
    _: *mut std::os::raw::c_void,
) -> u32 {
    error!("Vulkan: {}", CStr::from_ptr(p_message).to_string_lossy());
    0
}
