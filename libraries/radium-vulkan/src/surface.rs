use {
    std::ptr::{null},

    ash::{
        vk, Instance, Entry,
    },
    winit::{
        Window,
    },
};

#[cfg(windows)]
pub type SurfaceExtension = ::ash::extensions::Win32Surface;

#[cfg(unix)]
pub type SurfaceExtension = ::ash::extensions::XlibSurface;

#[cfg(windows)]
pub fn create_surface(
    entry: &Entry,
    instance: &Instance,
    window: &Window,
) -> vk::SurfaceKHR {
    use {
        ash::extensions::{Win32Surface},
        winit::os::windows::{WindowExt},
        winapi::{
            shared::windef::{HWND},
            um::winuser::{GetWindow},
        },
    };

    let hwnd = window.get_hwnd() as HWND;
    let hinstance = unsafe { GetWindow(hwnd, 0) } as *const std::ffi::c_void;
    let win32_create_info = vk::Win32SurfaceCreateInfoKHR {
        s_type: vk::StructureType::WIN32_SURFACE_CREATE_INFO_KHR,
        p_next: null(),
        flags: Default::default(),
        hinstance,
        hwnd: hwnd as *const std::ffi::c_void,
    };

    let win32_surface_ext = Win32Surface::new(entry, instance);
    unsafe { win32_surface_ext.create_win32_surface_khr(&win32_create_info, None) }.unwrap()
}

#[cfg(all(unix, not(target_os = "android")))]
pub fn create_surface(
    entry: &Entry,
    instance: &Instance,
    window: &Window,
) -> vk::SurfaceKHR {
    use {
        ash::extensions::{XlibSurface},
        winit::os::unix::{WindowExt},
    };

    let x11_display = window.get_xlib_display().unwrap();
    let x11_window = window.get_xlib_window().unwrap();
    let x11_create_info = vk::XlibSurfaceCreateInfoKHR {
        s_type: vk::StructureType::XLIB_SURFACE_CREATE_INFO_KHR,
        p_next: null(),
        flags: Default::default(),
        window: x11_window as vk::Window,
        dpy: x11_display as *mut vk::Display,
    };

    let xlib_surface_ext = XlibSurface::new(entry, instance);
    unsafe { xlib_surface_ext.create_xlib_surface_khr(&x11_create_info, None) }.unwrap()
}

#[cfg(target_os = "android")]
pub fn create_surface(
    entry: &Entry,
    instance: &Instance,
    window: &Window,
) -> vk::SurfaceKHR {
    use {
        ash::extensions::{AndroidSurface},
        winit::os::android::{WindowExt},
    };

    let native_window = window.get_native_window();
    let android_create_info = vk::AndroidSurfaceCreateInfoKHR {
        s_type: vk::StructureType::ANDROID_SURFACE_CREATE_INFO_KHR,
        p_next: null(),
        flags: Default::default(),
        window: native_window as vk::ANativeWindow,
    };

    let android_surface_ext = AndroidSurface::new(entry, instance);
    unsafe { android_surface_ext.create_android_surface_khr(&android_create_info, None) }.unwrap()
}
