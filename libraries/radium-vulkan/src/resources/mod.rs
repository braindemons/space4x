mod material;
mod mesh;
mod pipeline;
mod texture;

pub use self::{
    material::{Material},
    mesh::{Mesh},
    pipeline::{Pipeline},
    texture::{Texture},
};

use {
    std::ptr::{null},

    ash::{
        version::{DeviceV1_0},
        vk,
    },
    specs::{Resources, WriteExpect, SystemData},
    slotmap::{SecondaryMap},

    radium_3d::{
        resources::{
            PipelineId, MeshId, TextureId, MaterialId,
            PipelineResources, MeshResources, TextureResources, MaterialResources,
        },
    },

    crate::{
        device::{Device},
        uniforms_buffer::{UniformsBuffer},
    },
};

pub struct VulkanResources {
    descriptor_set_layout: vk::DescriptorSetLayout,
    pipeline_layout: vk::PipelineLayout,
    descriptor_pool: vk::DescriptorPool,

    meshes: SecondaryMap<MeshId, Mesh>,
    textures: SecondaryMap<TextureId, Texture>,
    materials: SecondaryMap<MaterialId, Material>,
    pipelines: SecondaryMap<PipelineId, Pipeline>,
}

impl VulkanResources {
    pub unsafe fn new(device: &Device) -> Self {
        let max_materials = 1024;

        let ub_layout_binding = vk::DescriptorSetLayoutBinding {
            binding: 0,
            descriptor_type: vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC,
            descriptor_count: 1,
            stage_flags: vk::ShaderStageFlags::VERTEX,
            p_immutable_samplers: null(),
        };

        let sampler_layout_binding = vk::DescriptorSetLayoutBinding {
            binding: 1,
            descriptor_type: vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
            descriptor_count: 1,
            stage_flags: vk::ShaderStageFlags::FRAGMENT,
            p_immutable_samplers: null(),
        };

        let bindings = [ub_layout_binding, sampler_layout_binding];
        let layout_info = vk::DescriptorSetLayoutCreateInfo {
            s_type: vk::StructureType::DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
            flags: Default::default(),
            p_next: null(),
            binding_count: bindings.len() as u32,
            p_bindings: bindings.as_ptr(),
        };

        let descriptor_set_layout =
            device.raw().create_descriptor_set_layout(&layout_info, None).unwrap();

        let layout_create_info = vk::PipelineLayoutCreateInfo {
            s_type: vk::StructureType::PIPELINE_LAYOUT_CREATE_INFO,
            p_next: null(),
            flags: Default::default(),
            set_layout_count: 1,
            p_set_layouts: &descriptor_set_layout,
            push_constant_range_count: 0,
            p_push_constant_ranges: null(),
        };

        let pipeline_layout =
            device.raw().create_pipeline_layout(&layout_create_info, None).unwrap();

        let pool_sizes = [
            vk::DescriptorPoolSize {
                ty: vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC,
                descriptor_count: max_materials,
            }, vk::DescriptorPoolSize {
                ty: vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
                descriptor_count: max_materials,
            }
        ];

        let pool_info = vk::DescriptorPoolCreateInfo {
            s_type: vk::StructureType::DESCRIPTOR_POOL_CREATE_INFO,
            p_next: null(),
            flags: Default::default(),
            max_sets: max_materials,
            pool_size_count: pool_sizes.len() as u32,
            p_pool_sizes: pool_sizes.as_ptr(),
        };

        let descriptor_pool =
            device.raw().create_descriptor_pool(&pool_info, None).unwrap();

        Self {
            descriptor_set_layout,
            pipeline_layout,
            descriptor_pool,

            meshes: SecondaryMap::new(),
            textures: SecondaryMap::new(),
            materials: SecondaryMap::new(),
            pipelines: SecondaryMap::new(),
        }
    }

    pub fn pipeline_layout(&self) -> vk::PipelineLayout {
        self.pipeline_layout
    }

    pub fn get_mesh(&self, mesh: MeshId) -> &Mesh {
        self.meshes.get(mesh).unwrap()
    }

    pub fn get_material(&self, material: MaterialId) -> &Material {
        self.materials.get(material).unwrap()
    }

    pub fn get_pipeline(&self, pipeline: PipelineId) -> &Pipeline {
        self.pipelines.get(pipeline).unwrap()
    }

    pub fn create_pending(
        &mut self,
        device: &Device, render_pass: vk::RenderPass, uniforms_buffer: &UniformsBuffer,
        res: &Resources,
    ) {
        // TODO: Right now internally these wait for the device to be entirely done rendering,
        // we can instead just bundle all resource uploads into a transient command buffer, and
        // use semaphores to have the render command buffer wait on the uploads to be completed.
        
        for (key, data) in WriteExpect::<PipelineResources>::fetch(res).drain_queue() {
            let pipeline = unsafe { Pipeline::new(
                &device, render_pass, self.pipeline_layout, &data.vert, &data.frag,
            ) };
            self.pipelines.insert(key, pipeline);
        }

        for (key, data) in WriteExpect::<MeshResources>::fetch(res).drain_queue() {
            let mesh = unsafe { Mesh::new(device, &data.vertices, &data.indices) };
            self.meshes.insert(key, mesh);
        }

        for (key, data) in WriteExpect::<TextureResources>::fetch(res).drain_queue() {
            let texture = unsafe { Texture::new(
                device,
                data.width, data.height, &data.blocks,
            ) };
            self.textures.insert(key, texture);
        }

        for (key, data) in WriteExpect::<MaterialResources>::fetch(res).drain_queue() {
            let texture = self.textures.get(data.base_color).unwrap();
            let material = unsafe { Material::new(
                device.raw(), self.descriptor_set_layout, uniforms_buffer, self.descriptor_pool,
                &texture,
            ) };
            self.materials.insert(key, material);
        }
    }

    pub unsafe fn destroy(&self, device: &Device) {
        for (_, pipeline) in &self.pipelines {
            pipeline.destroy(device);
        }
        for (_, mesh) in &self.meshes {
            mesh.destroy(device);
        }
        for (_, texture) in &self.textures {
            texture.destroy(device);
        }

        let device_raw = device.raw();
        device_raw.destroy_descriptor_pool(self.descriptor_pool, None);
        device_raw.destroy_pipeline_layout(self.pipeline_layout, None);
        device_raw.destroy_descriptor_set_layout(self.descriptor_set_layout, None);
    }
}