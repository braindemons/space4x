use {
    std::{ptr::{null}},

    ash::{version::{DeviceV1_0}, vk, Device},

    crate::{
        resources::{Texture},
        uniforms_buffer::{UniformsBuffer},
        Uniform,
    },
};

pub struct Material {
    pub descriptor_set: vk::DescriptorSet,
}

impl Material {
    pub unsafe fn new(
        device: &Device, descriptor_set_layout: vk::DescriptorSetLayout,
        uniforms_buffer: &UniformsBuffer, descriptor_pool: vk::DescriptorPool,
        base_color: &Texture,
    ) -> Self {
        let set_layouts = [descriptor_set_layout];
        let alloc_info = vk::DescriptorSetAllocateInfo {
            s_type: vk::StructureType::DESCRIPTOR_SET_ALLOCATE_INFO,
            p_next: null(),
            descriptor_pool,
            descriptor_set_count: 1,
            p_set_layouts: set_layouts.as_ptr(),
        };
        let descriptor_set =
            device.allocate_descriptor_sets(&alloc_info).unwrap()[0];

        let buffer_info = vk::DescriptorBufferInfo {
            buffer: uniforms_buffer.buffer.buffer,
            offset: 0,
            range: std::mem::size_of::<Uniform>() as u64,
        };

        let image_info = vk::DescriptorImageInfo {
            image_layout: vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
            image_view: base_color.view,
            sampler: base_color.sampler,
        };

        let descriptor_writes = [
            vk::WriteDescriptorSet {
                s_type: vk::StructureType::WRITE_DESCRIPTOR_SET,
                p_next: null(),
                dst_set: descriptor_set,
                dst_binding: 0,
                dst_array_element: 0,

                descriptor_count: 1,
                descriptor_type: vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC,

                p_image_info: null(),
                p_buffer_info: &buffer_info,
                p_texel_buffer_view: null(),
            }, vk::WriteDescriptorSet {
                s_type: vk::StructureType::WRITE_DESCRIPTOR_SET,
                p_next: null(),
                dst_set: descriptor_set,
                dst_binding: 1,
                dst_array_element: 0,

                descriptor_count: 1,
                descriptor_type: vk::DescriptorType::COMBINED_IMAGE_SAMPLER,

                p_image_info: &image_info,
                p_buffer_info: null(),
                p_texel_buffer_view: null(),
            },
        ];

        device.update_descriptor_sets(&descriptor_writes, &[]);

        Material {
            descriptor_set,
        }
    }
}
