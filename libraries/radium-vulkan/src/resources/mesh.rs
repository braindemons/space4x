use {
    ash::{vk},

    radium_3d::resources::{Vertex},

    crate::{
        device::{Device},
        buffer::{DedicatedMemoryBuffer, submit_buffer_copies},
    },
};

pub struct Mesh {
    pub vertex_buffer: DedicatedMemoryBuffer,
    pub index_buffer: DedicatedMemoryBuffer,
    pub indices: usize,
}

impl Mesh {
    pub unsafe fn new(
        device: &Device,
        vertices: &[Vertex], indices: &[u16],
    ) -> Self {
        // Set up vertex buffer
        let vertex_staging_buffer = DedicatedMemoryBuffer::staging_with_data(
            device, vertices
        );

        let vertex_buffer = DedicatedMemoryBuffer::new(
            device,
            vertex_staging_buffer.size,
            vk::BufferUsageFlags::TRANSFER_DST | vk::BufferUsageFlags::VERTEX_BUFFER,
            vk::MemoryPropertyFlags::DEVICE_LOCAL,
        );

        // Set up index buffer
        let index_staging_buffer = DedicatedMemoryBuffer::staging_with_data(
            device, &indices
        );

        let index_buffer = DedicatedMemoryBuffer::new(
            device,
            index_staging_buffer.size,
            vk::BufferUsageFlags::TRANSFER_DST | vk::BufferUsageFlags::INDEX_BUFFER,
            vk::MemoryPropertyFlags::DEVICE_LOCAL,
        );

        submit_buffer_copies(
            device, &[
                (&vertex_staging_buffer, &vertex_buffer),
                (&index_staging_buffer, &index_buffer),
            ],
        );

        vertex_staging_buffer.destroy(device);
        index_staging_buffer.destroy(device);

        Mesh {
            vertex_buffer,
            index_buffer,
            indices: indices.len(),
        }
    }
    
    pub unsafe fn destroy(&self, device: &Device) {
        self.vertex_buffer.destroy(device);
        self.index_buffer.destroy(device);
    }
}
