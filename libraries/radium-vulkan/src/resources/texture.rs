use {
    std::{ptr},

    ash::{version::{DeviceV1_0}, vk},

    crate::{
        device::{Device},
        buffer::{DedicatedMemoryBuffer},
        image::{
            create_image, transition_image_layout, copy_buffer_to_image, create_image_view
        },
    },
};

pub struct Texture {
    image: vk::Image,
    memory: vk::DeviceMemory,
    pub view: vk::ImageView,
    pub sampler: vk::Sampler,
}

impl Texture {
    pub unsafe fn new(
        device: &Device,
        width: u32, height: u32, image_data: &[u8],
    ) -> Self {
        let format = vk::Format::BC1_RGB_SRGB_BLOCK;

        let (image, memory, _memory_requirements) = create_image(
            device, width, height, format,
            vk::ImageUsageFlags::TRANSFER_DST | vk::ImageUsageFlags::SAMPLED,
        );

        let staging = DedicatedMemoryBuffer::staging_with_data(
            device, image_data,
        );

        transition_image_layout(
            device, image,
            format,
            vk::ImageLayout::UNDEFINED,
            vk::ImageLayout::TRANSFER_DST_OPTIMAL,
        );
        copy_buffer_to_image(
            device,
            staging.buffer, image, width, height,
        );
        transition_image_layout(
            device, image,
            format,
            vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
        );

        staging.destroy(device);

        let view = create_image_view(device, image, format, vk::ImageAspectFlags::COLOR);

        let sampler = create_sampler(device);

        Texture {
            image,
            memory,
            view,
            sampler,
        }
    }

    
    pub unsafe fn destroy(&self, device: &Device) {
        let device = device.raw();

        device.destroy_sampler(self.sampler, None);
        device.destroy_image_view(self.view, None);
        device.destroy_image(self.image, None);
        device.free_memory(self.memory, None);
    }
}

unsafe fn create_sampler(device: &Device) -> vk::Sampler {
    let sampler_info = vk::SamplerCreateInfo {
        s_type: vk::StructureType::SAMPLER_CREATE_INFO,
        p_next: ptr::null(),
        flags: Default::default(),
        mag_filter: vk::Filter::LINEAR,
        min_filter: vk::Filter::LINEAR,

        address_mode_u: vk::SamplerAddressMode::REPEAT,
        address_mode_v: vk::SamplerAddressMode::REPEAT,
        address_mode_w: vk::SamplerAddressMode::REPEAT,

        // TODO: Check feature support
        anisotropy_enable: vk::TRUE,
        max_anisotropy: 16.0,

        border_color: vk::BorderColor::INT_OPAQUE_BLACK,

        unnormalized_coordinates: vk::FALSE,

        compare_enable: vk::FALSE,
        compare_op: vk::CompareOp::ALWAYS,

        mipmap_mode: vk::SamplerMipmapMode::LINEAR,
        mip_lod_bias: 0.0,
        min_lod: 0.0,
        max_lod: 0.0,
    };

    device.raw().create_sampler(&sampler_info, None).unwrap()
}
