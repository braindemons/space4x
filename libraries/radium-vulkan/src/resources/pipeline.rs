use {
    std::{ffi::{CString}, ptr::{null}, mem},

    ash::{
        version::{DeviceV1_0},
        vk,
    },

    radium_3d::resources::{Vertex},

    crate::{
        device::{Device},
    },
};

pub struct Pipeline {
    vertex_shader: vk::ShaderModule,
    fragment_shader: vk::ShaderModule,
    raw: vk::Pipeline,
}

impl Pipeline {
    pub unsafe fn new(
        device: &Device,
        render_pass: vk::RenderPass, pipeline_layout: vk::PipelineLayout,
        vert: &[u32],
        frag: &[u32],
    ) -> Self {
        // Load in shader files
        let vertex_shader = load_shader(device, vert);
        let fragment_shader = load_shader(device, frag);

        let raw = create_pipeline(
            device,
            vertex_shader, fragment_shader,
            render_pass, pipeline_layout,
        );

        Pipeline {
            vertex_shader,
            fragment_shader,
            raw,
        }
    }

    pub fn raw(&self) -> vk::Pipeline {
        self.raw
    }
    
    pub unsafe fn destroy(&self, device: &Device) {
        let device = device.raw();

        device.destroy_pipeline(self.raw, None);
        device.destroy_shader_module(self.vertex_shader, None);
        device.destroy_shader_module(self.fragment_shader, None);
    }
}

fn load_shader(
    device: &Device, spirv: &[u32],
) -> vk::ShaderModule {
    let module_info = vk::ShaderModuleCreateInfo {
        s_type: vk::StructureType::SHADER_MODULE_CREATE_INFO,
        p_next: null(),
        flags: Default::default(),
        code_size: spirv.len()*4,
        p_code: spirv.as_ptr(),
    };

    unsafe { device.raw().create_shader_module(&module_info, None) }.unwrap()
}

fn create_pipeline(
    device: &Device,
    vertex_shader: vk::ShaderModule, fragment_shader: vk::ShaderModule,
    render_pass: vk::RenderPass, pipeline_layout: vk::PipelineLayout,
) -> vk::Pipeline {
    let entry_name = CString::new("main").unwrap();
    let vertex_stage = vk::PipelineShaderStageCreateInfo {
        s_type: vk::StructureType::PIPELINE_SHADER_STAGE_CREATE_INFO,
        p_next: null(),
        flags: Default::default(),
        module: vertex_shader,
        p_name: entry_name.as_ptr(),
        p_specialization_info: null(),
        stage: vk::ShaderStageFlags::VERTEX,
    };
    let fragment_stage = vk::PipelineShaderStageCreateInfo {
        s_type: vk::StructureType::PIPELINE_SHADER_STAGE_CREATE_INFO,
        p_next: null(),
        flags: Default::default(),
        module: fragment_shader,
        p_name: entry_name.as_ptr(),
        p_specialization_info: null(),
        stage: vk::ShaderStageFlags::FRAGMENT,
    };
    let shader_stages = [vertex_stage, fragment_stage];
    
    let binding_description = vk::VertexInputBindingDescription {
        binding: 0,
        stride: mem::size_of::<Vertex>() as u32,
        input_rate: vk::VertexInputRate::VERTEX,
    };

    let attribute_descriptions = [
        vk::VertexInputAttributeDescription {
            location: 0,
            binding: 0,
            format: vk::Format::R32G32B32_SFLOAT,
            offset: 0,
        }, vk::VertexInputAttributeDescription {
            location: 1,
            binding: 0,
            format: vk::Format::R32G32B32_SFLOAT,
            offset: 12 as u32,
        }, vk::VertexInputAttributeDescription {
            location: 2,
            binding: 0,
            format: vk::Format::R32G32_SFLOAT,
            offset: 24 as u32,
        }
    ];

    let vertex_input_state_info = vk::PipelineVertexInputStateCreateInfo {
        s_type: vk::StructureType::PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        p_next: null(),
        flags: Default::default(),
        vertex_binding_description_count: 1,
        p_vertex_binding_descriptions: &binding_description,
        vertex_attribute_description_count: attribute_descriptions.len() as u32,
        p_vertex_attribute_descriptions: attribute_descriptions.as_ptr(),
    };

    let input_assembly_state_info = vk::PipelineInputAssemblyStateCreateInfo {
        s_type: vk::StructureType::PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        flags: Default::default(),
        p_next: null(),
        topology: vk::PrimitiveTopology::TRIANGLE_LIST,
        primitive_restart_enable: 0,
    };

    let viewport_state_info = vk::PipelineViewportStateCreateInfo {
        s_type: vk::StructureType::PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        p_next: null(),
        flags: Default::default(),
        scissor_count: 1,
        p_scissors: null(),
        viewport_count: 1,
        p_viewports: null(),
    };

    let rasterizer_state_info = vk::PipelineRasterizationStateCreateInfo {
        s_type: vk::StructureType::PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        p_next: null(),
        flags: Default::default(),
        depth_clamp_enable: 0,
        rasterizer_discard_enable: 0,
        polygon_mode: vk::PolygonMode::FILL,
        cull_mode: vk::CullModeFlags::BACK,
        front_face: vk::FrontFace::COUNTER_CLOCKWISE,
        depth_bias_enable: 0,
        depth_bias_constant_factor: 0.0,
        depth_bias_clamp: 0.0,
        depth_bias_slope_factor: 0.0,
        line_width: 1.0,
    };

    let multisample_state_info = vk::PipelineMultisampleStateCreateInfo {
        s_type: vk::StructureType::PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        p_next: null(),
        flags: Default::default(),
        rasterization_samples: vk::SampleCountFlags::TYPE_1,
        sample_shading_enable: 0,
        min_sample_shading: 1.0,
        p_sample_mask: null(),
        alpha_to_one_enable: 0,
        alpha_to_coverage_enable: 0,
    };

    let depth_stencil_state = vk::PipelineDepthStencilStateCreateInfo {
        s_type: vk::StructureType::PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        p_next: null(),
        flags: Default::default(),
        depth_test_enable: vk::TRUE,
        depth_write_enable: vk::TRUE,

        depth_compare_op: vk::CompareOp::LESS,

        depth_bounds_test_enable: vk::FALSE,
        min_depth_bounds: 0.0,
        max_depth_bounds: 1.0,

        stencil_test_enable: vk::FALSE,
        // front & back are just set to 0 values
        front: vk::StencilOpState {
            fail_op: vk::StencilOp::KEEP,
            pass_op: vk::StencilOp::KEEP,
            depth_fail_op: vk::StencilOp::KEEP,
            compare_op: vk::CompareOp::NEVER,
            compare_mask: 0,
            write_mask: 0,
            reference: 0,
        },
        back: vk::StencilOpState {
            fail_op: vk::StencilOp::KEEP,
            pass_op: vk::StencilOp::KEEP,
            depth_fail_op: vk::StencilOp::KEEP,
            compare_op: vk::CompareOp::NEVER,
            compare_mask: 0,
            write_mask: 0,
            reference: 0,
        },
    };

    let color_blend_attachment_state = vk::PipelineColorBlendAttachmentState {
        blend_enable: 0,
        src_color_blend_factor: vk::BlendFactor::ONE,
        dst_color_blend_factor: vk::BlendFactor::ZERO,
        color_blend_op: vk::BlendOp::ADD,
        src_alpha_blend_factor: vk::BlendFactor::ONE,
        dst_alpha_blend_factor: vk::BlendFactor::ZERO,
        alpha_blend_op: vk::BlendOp::ADD,
        color_write_mask: vk::ColorComponentFlags::all(),
    };
    let color_blend_state_info = vk::PipelineColorBlendStateCreateInfo {
        s_type: vk::StructureType::PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        p_next: null(),
        flags: Default::default(),
        logic_op_enable: 0,
        logic_op: vk::LogicOp::COPY,
        attachment_count: 1,
        p_attachments: &color_blend_attachment_state,
        blend_constants: [0.0, 0.0, 0.0, 0.0],
    };

    let dynamic_state = [vk::DynamicState::VIEWPORT, vk::DynamicState::SCISSOR];
    let dynamic_state_info = vk::PipelineDynamicStateCreateInfo {
        s_type: vk::StructureType::PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        p_next: null(),
        flags: Default::default(),
        dynamic_state_count: dynamic_state.len() as u32,
        p_dynamic_states: dynamic_state.as_ptr(),
    };

    let graphics_pipeline_info = vk::GraphicsPipelineCreateInfo {
        s_type: vk::StructureType::GRAPHICS_PIPELINE_CREATE_INFO,
        p_next: null(),
        flags: Default::default(),
        stage_count: 2,
        p_stages: shader_stages.as_ptr(),
        p_vertex_input_state: &vertex_input_state_info,
        p_input_assembly_state: &input_assembly_state_info,
        p_tessellation_state: null(),
        p_viewport_state: &viewport_state_info,
        p_rasterization_state: &rasterizer_state_info,
        p_multisample_state: &multisample_state_info,
        p_depth_stencil_state: &depth_stencil_state,
        p_color_blend_state: &color_blend_state_info,
        p_dynamic_state: &dynamic_state_info,
        layout: pipeline_layout,
        render_pass,
        subpass: 0,
        base_pipeline_handle: vk::Pipeline::null(),
        base_pipeline_index: 0,
    };

    unsafe {
        device.raw().create_graphics_pipelines(
            vk::PipelineCache::null(), &[graphics_pipeline_info], None
        ).unwrap()[0]
    }
}