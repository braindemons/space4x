mod device;
mod resources;

mod allocator;
mod buffer;
mod debug;
mod image;
mod instance;
mod memory;
mod surface;
mod swapchain;
mod uniforms_buffer;

use {
    std::{
        ptr::{null},
    },

    ash::{
        extensions::{Swapchain as VkSwapchain, DebugReport, Surface},
        version::{InstanceV1_0, DeviceV1_0},
        vk, Entry, Instance,
    },
    log::{info, warn},
    nalgebra::{Matrix4},
    specs::{ReadStorage, Join, Resources, SystemData},
    winit::{Window},

    radium::{Renderer},
    radium_3d::{
        components::{CameraComponent, MeshComponent, MatrixComponent},
        resources::{PipelineResources, MeshResources, TextureResources, MaterialResources},
    },

    crate::{
        device::{Device},
        resources::{VulkanResources},
        allocator::{Allocator},
        swapchain::{SwapchainManager},
        uniforms_buffer::{UniformsBuffer},
    },
};

#[repr(C)]
#[derive(Clone, Copy)]
pub struct Uniform {
    matrix_total: Matrix4<f32>,
    matrix_normal: Matrix4<f32>,
}

type RenderData<'a> = (
    ReadStorage<'a, CameraComponent>,
    ReadStorage<'a, MeshComponent>,
    ReadStorage<'a, MatrixComponent>,
);

pub struct VulkanRenderer {
    _entry: Entry,
    instance: Instance,
    debug: Option<(DebugReport, vk::DebugReportCallbackEXT)>,

    surface_ext: Surface,
    surface: vk::SurfaceKHR,

    device: Device,
    allocator: Allocator,
    render_pass: vk::RenderPass,
    swapchain_manager: SwapchainManager,

    // In-flight frame data
    in_flight_index: usize,
    in_flight_frames: Vec<InFlightFrame>,
    uniforms_buffer: UniformsBuffer,

    resources: VulkanResources,
}

pub struct InFlightFrame {
    // Synchronization
    /// Triggered by `acquire_next_image_khr` when the image is aquired and ready for rendering,
    /// waited on by the command buffer before starting to render to it.
    acquire_complete: vk::Semaphore,
    /// Triggered by the command buffer submission when done executing, waited on by
    /// `queue_present_khr` before showing the image.
    render_complete: vk::Semaphore,
    /// Lets us wait till the in-flight frame is completely done before attempting to render the
    /// next one.
    frame_complete: vk::Fence,

    // Frame slot resources
    command_buffer: vk::CommandBuffer,
    uniforms_offset: u32,
}

impl VulkanRenderer {
    pub fn new(window: &Window, app_name: &str) -> Self {
        info!("Creating Vulkan Renderer");

        let entry: Entry = Entry::new().unwrap();
        let mut validation = cfg!(debug_assertions);

        // Create the vulkan instance
        let instance = instance::create_instance(&entry, app_name, &mut validation);

        // If we have everything set up for validation, add a callback for it
        let debug = if validation {
            Some(debug::create_callback(&entry, &instance))
        } else {
            None
        };

        // Create the surface we'll have to swap the final image to, the window's surface
        let surface_ext = Surface::new(&entry, &instance);
        let surface = surface::create_surface(&entry, &instance, window);

        // We need at least these extensions for basic rendering functionality
        let required_device_extensions = vec![
            VkSwapchain::name().as_ptr()
        ];

        let physical_device = device::find_best(
            &instance, &surface_ext, surface, &required_device_extensions,
        );

        // Create the logical device and graphics queue, which lets us actually communicate with
        // the GPU
        let device = unsafe { Device::new(
            &instance, &surface_ext, surface, physical_device,
            &required_device_extensions, debug.is_some(),
        ) };

        let allocator = unsafe { Allocator::new(&device) };

        let present_mode = device.physical_device().find_present_mode(&surface_ext, surface);
        let surface_format = device.physical_device().find_surface_format(&surface_ext, surface);
        info!("Present Mode {}", present_mode);
        info!("Surface Format {}", surface_format.format);
        info!("Surface Color Space {}", surface_format.color_space);

        let render_pass = create_render_pass(&device, surface_format);

        let swapchain_manager = unsafe { SwapchainManager::new(
            &instance, &device,
            present_mode, surface_format,
        ) };

        let in_flight_frame_count = 2;
        let uniforms_per_slot = 8192;

        let in_flight_frames = create_in_flight_frames(
            &device, in_flight_frame_count, uniforms_per_slot,
        );

        let uniforms_buffer = unsafe { UniformsBuffer::new(
            &device,
            in_flight_frame_count, uniforms_per_slot,
        ) };

        let resources = unsafe { VulkanResources::new(&device) };

        Self {
            _entry: entry,
            instance,
            debug,

            surface_ext,
            surface,

            device,
            allocator,
            render_pass,
            swapchain_manager,

            in_flight_index: 0,
            in_flight_frames,
            uniforms_buffer,

            resources,
        }
    }

    unsafe fn begin_rendering(
        &self,
        frame: &InFlightFrame, extent: vk::Extent2D, framebuffer: vk::Framebuffer,
    ) {
        let device_raw = self.device.raw();

        // We only allow a limited amount of frames to be in-flight at a time, so wait until our
        // frame's slot has finished rendering.
        device_raw.wait_for_fences(
            &[frame.frame_complete], true, std::u64::MAX,
        ).unwrap();
        device_raw.reset_fences(&[frame.frame_complete]).unwrap();

        let buffer_begin_info = vk::CommandBufferBeginInfo {
            s_type: vk::StructureType::COMMAND_BUFFER_BEGIN_INFO,
            p_next: null(),
            flags: vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
            p_inheritance_info: null(),
        };
        device_raw.begin_command_buffer(frame.command_buffer, &buffer_begin_info).unwrap();

        let clear_values = [
            vk::ClearValue {
                color: vk::ClearColorValue { float32: [0.0, 0.0, 0.0, 1.0] },
            }, vk::ClearValue {
                depth_stencil: vk::ClearDepthStencilValue { depth: 1.0, stencil: 0 },
            },
        ];

        let render_pass_begin_info = vk::RenderPassBeginInfo {
            s_type: vk::StructureType::RENDER_PASS_BEGIN_INFO,
            p_next: null(),
            render_pass: self.render_pass,
            framebuffer,
            render_area: vk::Rect2D {
                offset: vk::Offset2D { x: 0, y: 0 },
                extent,
            },
            clear_value_count: clear_values.len() as u32,
            p_clear_values: clear_values.as_ptr(),
        };
        device_raw.cmd_begin_render_pass(
            frame.command_buffer, &render_pass_begin_info, vk::SubpassContents::INLINE,
        );

        let viewport = vk::Viewport {
            x: 0.0,
            y: 0.0,
            width: extent.width as f32,
            height: extent.height as f32,
            min_depth: 0.0,
            max_depth: 1.0,
        };
        device_raw.cmd_set_viewport(frame.command_buffer, 0, &[viewport]);

        let scissor = vk::Rect2D {
            offset: vk::Offset2D { x: 0, y: 0 },
            extent,
        };
        device_raw.cmd_set_scissor(frame.command_buffer, 0, &[scissor]);
    }

    unsafe fn finish_and_submit(&self, frame: &InFlightFrame) {
        let device_raw = self.device.raw();

        device_raw.cmd_end_render_pass(frame.command_buffer);

        device_raw.end_command_buffer(frame.command_buffer).unwrap();

        // Submit everything we've recorded to the command buffer to the graphics queue
        let wait_stages = [vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT];
        let submit_info = vk::SubmitInfo {
            s_type: vk::StructureType::SUBMIT_INFO,
            p_next: null(),
            wait_semaphore_count: 1,
            p_wait_semaphores: &frame.acquire_complete,
            p_wait_dst_stage_mask: wait_stages.as_ptr(),
            command_buffer_count: 1,
            p_command_buffers: &frame.command_buffer,
            signal_semaphore_count: 1,
            p_signal_semaphores: &frame.render_complete,
        };

        device_raw.queue_submit(
            self.device.graphics_queue(),
            &[submit_info],
            frame.frame_complete,
        ).unwrap();
    }

    fn render_mesh(
        &self,
        frame: &InFlightFrame,
        uniforms: &mut Vec<Uniform>,
        projection_view: &Matrix4<f32>,
        matrix_component: &MatrixComponent,
        mesh_component: &MeshComponent,
    ) {
        // Make sure we've got uniform slots left
        if uniforms.len() >= self.uniforms_buffer.uniforms_limit {
            warn!("Exceeded visible mesh limit");
            return
        }

        // Add the uniform buffer's data (will be copied to the buffer later)
        let uniform = Uniform {
            matrix_total: projection_view * matrix_component.matrix,
            matrix_normal: matrix_component.matrix.try_inverse().unwrap().transpose(),
        };
        uniforms.push(uniform);

        // Calculate the offset in the buffer this uniform will have
        let uniform_index = (uniforms.len() - 1) as u32;
        let local_offset = uniform_index * self.uniforms_buffer.uniform_padded_size as u32;
        let uniform_offset = frame.uniforms_offset + local_offset;

        // Find the resources for the component
        let mesh = self.resources.get_mesh(mesh_component.mesh);
        let material = self.resources.get_material(mesh_component.material);
        let pipeline = self.resources.get_pipeline(mesh_component.pipeline);

        unsafe {
            let device_raw = self.device.raw();

            device_raw.cmd_bind_pipeline(
                frame.command_buffer, vk::PipelineBindPoint::GRAPHICS,
                pipeline.raw(),
            );

            device_raw.cmd_bind_descriptor_sets(
                frame.command_buffer, vk::PipelineBindPoint::GRAPHICS,
                self.resources.pipeline_layout(), 0, &[material.descriptor_set],
                &[uniform_offset],
            );

            device_raw.cmd_bind_vertex_buffers(
                frame.command_buffer, 0, &[mesh.vertex_buffer.buffer], &[0],
            );
            device_raw.cmd_bind_index_buffer(
                frame.command_buffer, mesh.index_buffer.buffer, 0,
                vk::IndexType::UINT16,
            );

            device_raw.cmd_draw_indexed(
                frame.command_buffer, mesh.indices as u32, 1, 0, 0, 0
            );
        }
    }
}

impl Renderer for VulkanRenderer {
    fn setup(&mut self, res: &mut Resources) {
        RenderData::setup(res);

        // These are the resources this backend supports
        res.insert(PipelineResources::new());
        res.insert(MeshResources::new());
        res.insert(TextureResources::new());
        res.insert(MaterialResources::new());
    }

    fn render(&mut self, res: &Resources) {
        // Create all pending registered resources
        self.resources.create_pending(&self.device, self.render_pass, &self.uniforms_buffer, res);

        // Actually render, this is in a loop to retry swapchain calls failed because of resizes
        loop {
            let frame = &self.in_flight_frames[self.in_flight_index];

            self.swapchain_manager.maintain(
                &self.device,
                &self.surface_ext, self.surface,
                self.render_pass,
            );

            // Aquire an image for the swapchain for use in this frame
            let result = self.swapchain_manager.acquire(frame.acquire_complete);
            let (swapchain, extent, framebuffer, image_index) = match result {
                Some(data) => data,
                None => continue,
            };

            unsafe {
                self.begin_rendering(frame, extent, framebuffer);
            }

            let mut uniforms = Vec::new();

            let (cameras, meshes, matrices) = RenderData::fetch(res);
            match (&matrices, &cameras).join().next() {
                Some(camera) => {
                    // Calculate the final matrix for the camera
                    let view_inverse = camera.0.matrix;
                    let projection = calculate_projection(extent);
                    let projection_view = projection * view_inverse.try_inverse().unwrap();

                    for (mesh, matrix) in (&meshes, &matrices).join() {
                        self.render_mesh(frame, &mut uniforms, &projection_view, matrix, mesh);
                    }
                },
                None => panic!("No camera in world"),
            }

            unsafe { 
                // Copy over the gathered uniform data to our slot in the the uniforms buffer, so
                // it can be used while rendering.
                self.uniforms_buffer.copy_to_buffer(
                    &self.device, &uniforms, frame.uniforms_offset,
                );

                self.finish_and_submit(frame);
            }

            self.swapchain_manager.present(
                self.device.graphics_queue(), swapchain,
                frame.render_complete, image_index,
            );

            self.in_flight_index = (self.in_flight_index + 1) % self.in_flight_frames.len();

            break
        }
    }
}

impl Drop for VulkanRenderer {
    fn drop(&mut self) {
        unsafe {
            let device_raw = &self.device.raw();

            device_raw.device_wait_idle().unwrap();

            self.resources.destroy(&self.device);

            self.uniforms_buffer.destroy(&self.device);

            // Destroy in-flight frame resources
            let mut command_buffers = Vec::new();

            for frame in &self.in_flight_frames {
                device_raw.destroy_fence(frame.frame_complete, None);
                device_raw.destroy_semaphore(
                    frame.render_complete, None
                );
                device_raw.destroy_semaphore(
                    frame.acquire_complete, None
                );

                command_buffers.push(frame.command_buffer);
            }

            device_raw.free_command_buffers(
                self.device.resetting_command_pool(),
                &command_buffers,
            );

            self.swapchain_manager.destroy(&self.device);

            device_raw.destroy_render_pass(self.render_pass, None);

            self.allocator.destroy(&self.device);

            self.device.destroy();

            self.surface_ext.destroy_surface_khr(self.surface, None);

            if let Some((ref debug_report_loader, callback)) = self.debug {
                debug_report_loader.destroy_debug_report_callback_ext(callback, None);
            }

            self.instance.destroy_instance(None);
        }
    }
}

fn create_render_pass(
    device: &Device, surface_format: vk::SurfaceFormatKHR,
) -> vk::RenderPass {
    let color_attachment = vk::AttachmentDescription {
        flags: vk::AttachmentDescriptionFlags::empty(),
        format: surface_format.format,
        samples: vk::SampleCountFlags::TYPE_1,
        load_op: vk::AttachmentLoadOp::CLEAR,
        store_op: vk::AttachmentStoreOp::STORE,
        stencil_load_op: vk::AttachmentLoadOp::DONT_CARE,
        stencil_store_op: vk::AttachmentStoreOp::DONT_CARE,
        initial_layout: vk::ImageLayout::UNDEFINED,
        final_layout: vk::ImageLayout::PRESENT_SRC_KHR,
    };

    let color_attachment_reference = vk::AttachmentReference {
        attachment: 0,
        layout: vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
    };

    let depth_attachment = vk::AttachmentDescription {
        flags: vk::AttachmentDescriptionFlags::empty(),
        format: vk::Format::D32_SFLOAT,
        samples: vk::SampleCountFlags::TYPE_1,
        load_op: vk::AttachmentLoadOp::CLEAR,
        store_op: vk::AttachmentStoreOp::DONT_CARE,
        stencil_load_op: vk::AttachmentLoadOp::DONT_CARE,
        stencil_store_op: vk::AttachmentStoreOp::DONT_CARE,
        initial_layout: vk::ImageLayout::UNDEFINED,
        final_layout: vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
    };

    let depth_attachment_reference = vk::AttachmentReference {
        attachment: 1,
        layout: vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
    };

    let subpass = vk::SubpassDescription {
        flags: Default::default(),
        pipeline_bind_point: vk::PipelineBindPoint::GRAPHICS,
        input_attachment_count: 0,
        p_input_attachments: null(),
        color_attachment_count: 1,
        p_color_attachments: &color_attachment_reference,
        p_resolve_attachments: null(),
        p_depth_stencil_attachment: &depth_attachment_reference,
        preserve_attachment_count: 0,
        p_preserve_attachments: null(),
    };

    let dependency = vk::SubpassDependency {
        src_subpass: vk::SUBPASS_EXTERNAL,
        dst_subpass: 0,
        src_stage_mask: vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
        dst_stage_mask: vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
        src_access_mask: Default::default(),
        dst_access_mask:
            vk::AccessFlags::COLOR_ATTACHMENT_READ |
            vk::AccessFlags::COLOR_ATTACHMENT_WRITE,
        dependency_flags: Default::default(),
    };

    let attachments = [color_attachment, depth_attachment];
    let renderpass_info = vk::RenderPassCreateInfo {
        s_type: vk::StructureType::RENDER_PASS_CREATE_INFO,
        p_next: null(),
        flags: Default::default(),
        attachment_count: attachments.len() as u32,
        p_attachments: attachments.as_ptr(),
        subpass_count: 1,
        p_subpasses: &subpass,
        dependency_count: 1,
        p_dependencies: &dependency,
    };

    unsafe {
        device.raw().create_render_pass(&renderpass_info, None).unwrap()
    }
}

fn create_in_flight_frames(
    device: &Device, in_flight_frame_count: usize, uniforms_per_slot: usize,
) -> Vec<InFlightFrame> {
    let semaphore_info = vk::SemaphoreCreateInfo {
        s_type: vk::StructureType::SEMAPHORE_CREATE_INFO,
        p_next: null(),
        flags: Default::default(),
    };
    let fence_info = vk::FenceCreateInfo {
        s_type: vk::StructureType::FENCE_CREATE_INFO,
        p_next: null(),
        flags: vk::FenceCreateFlags::SIGNALED,
    };

    let command_buffer_info = vk::CommandBufferAllocateInfo {
        s_type: vk::StructureType::COMMAND_BUFFER_ALLOCATE_INFO,
        p_next: null(),
        command_pool: device.resetting_command_pool(),
        level: vk::CommandBufferLevel::PRIMARY,
        command_buffer_count: in_flight_frame_count as u32,
    };
    let command_buffers = unsafe {
        device.raw().allocate_command_buffers(&command_buffer_info).unwrap()
    };

    let mut in_flight_frames = Vec::new();
    #[allow(clippy::needless_range_loop)]
    for i in 0..in_flight_frame_count {
        unsafe {
            let acquire_complete =
                device.raw().create_semaphore(&semaphore_info, None).unwrap();
            let render_complete =
                device.raw().create_semaphore(&semaphore_info, None).unwrap();
            let frame_complete =
                device.raw().create_fence(&fence_info, None).unwrap();

            in_flight_frames.push(InFlightFrame {
                acquire_complete,
                render_complete,
                frame_complete,

                command_buffer: command_buffers[i],
                uniforms_offset: uniforms_per_slot as u32 * i as u32,
            });
        }
    }

    in_flight_frames
}

fn calculate_projection(extent: vk::Extent2D) -> Matrix4<f32> {
    let h_fov = ::std::f32::consts::PI / 2.0; // 90 deg
    let v_fov = horizontal_to_vertical_fov(h_fov, extent);

    let ratio = extent.width as f32 / extent.height as f32;
    let mut projection = Matrix4::new_perspective(ratio, v_fov, 0.1, 1000.0);

    // Convert this matrix from OpenGL to Vulkan
    projection.m22 *= -1.0;

    projection
}

fn horizontal_to_vertical_fov(h_fov: f32, extent: vk::Extent2D) -> f32 {
    let fov_ratio = extent.height as f32 / extent.width as f32;
    2.0 * ((h_fov/2.0).tan() * fov_ratio).atan()
}
