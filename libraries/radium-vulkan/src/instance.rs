use {
    std::{
        ffi::{CString, CStr},
        ptr,
    },
    
    ash::{
        extensions::{Surface, DebugReport},
        version::{EntryV1_0},
        vk, Entry, Instance, vk_make_version,
    },
    log::{info, warn},
};

pub fn create_instance(
    entry: &Entry, app_name: &str, validation: &mut bool,
) -> Instance {
    info!("Creating Vulkan Instance");

    let layers = detect_layers(entry, validation);
    let extensions = detect_extensions(entry, validation);

    // Prepare information for creating the instance itself
    let app_name = CString::new(app_name).unwrap();
    let engine_name = CString::new("radium").unwrap();
    let app_info = vk::ApplicationInfo {
        s_type: vk::StructureType::APPLICATION_INFO,
        p_next: ptr::null(),
        p_application_name: app_name.as_ptr(),
        application_version: 0,
        p_engine_name: engine_name.as_ptr(),
        engine_version: 0,
        api_version: vk_make_version!(1, 0, 36),
    };

    let create_info = vk::InstanceCreateInfo {
        s_type: vk::StructureType::INSTANCE_CREATE_INFO,
        p_next: ptr::null(),
        flags: Default::default(),
        p_application_info: &app_info,
        enabled_layer_count: layers.len() as u32,
        pp_enabled_layer_names: layers.as_ptr(),
        enabled_extension_count: extensions.len() as u32,
        pp_enabled_extension_names: extensions.as_ptr(),
    };

    // Create the instance
    unsafe { entry.create_instance(&create_info, None) }.unwrap()
}

fn detect_layers(entry: &Entry, validation: &mut bool) -> Vec<*const i8> {
    // Find all available layers, checking if the validation layer is available
    let mut validation_layer_available = false;
    let lunarch_validation =
        CStr::from_bytes_with_nul(b"VK_LAYER_LUNARG_standard_validation\0").unwrap();

    let mut text = String::new();
    let layers = entry.enumerate_instance_layer_properties().unwrap();
    for layer in layers {
        let name = unsafe { CStr::from_ptr(&layer.layer_name as *const i8) };
        text.push_str(&name.to_string_lossy());
        text.push_str(", ");

        if name == lunarch_validation {
            validation_layer_available = true;
        }
    }
    if !text.is_empty() {
        info!("Available Layers\n{}", &text[..text.len()-2]);
    } else {
        info!("No Layers Available");
    }

    let mut layers = Vec::new();

    // Make use of validation layers if debug_assertions has been enabled
    if *validation {
        if validation_layer_available {
            info!("Enabling Validation Layer");
            layers.push(lunarch_validation.as_ptr());
        } else {
            warn!("Validation Layer not available, install the LunarG SDK");
            *validation = false;
        }
    } else {
        info!("Disabling Validation Layer");
    }

    layers
}

fn detect_extensions(entry: &Entry, validation: &mut bool) -> Vec<*const i8> {
    // Find all available extensions, checking if the debug report extension is available
    let mut debug_report_available = false;

    let mut text = String::new();
    let extensions = entry.enumerate_instance_extension_properties().unwrap();
    for extension in extensions {
        let name = unsafe { CStr::from_ptr(&extension.extension_name as *const i8) };
        text.push_str(&name.to_string_lossy());
        text.push_str(", ");

        if name == DebugReport::name() {
            debug_report_available = true;
        }
    }
    if !text.is_empty() {
        info!("Available Extensions\n{}", &text[..text.len()-2]);
    } else {
        info!("No Extensions Available");
    }

    // We need these extensions anyways to render to winit
    let mut extensions = vec![
        Surface::name().as_ptr(),
        crate::surface::SurfaceExtension::name().as_ptr(),
    ];

    // Add the debug report extension if we're enabling validation and the extension is available
    if *validation {
        if debug_report_available {
            info!("Enabling Debug Report Extension");
            extensions.push(DebugReport::name().as_ptr());
        } else {
            warn!("Debug report extension not available, install the LunarG SDK");
            *validation = false;
        }
    } else {
        info!("Disabling Debug Report Extension");
    }

    extensions
}
