use {
    std::{ptr::{null}},

    ash::{version::{DeviceV1_0}, vk},

    crate::device::{Device},
};

pub fn begin_transient(
    device: &Device,
) -> vk::CommandBuffer {
    unsafe {
        let command_buffer_info = vk::CommandBufferAllocateInfo {
            s_type: vk::StructureType::COMMAND_BUFFER_ALLOCATE_INFO,
            p_next: null(),
            command_pool: device.transient_command_pool(),
            level: vk::CommandBufferLevel::PRIMARY,
            command_buffer_count: 1,
        };
        let command_buffer =
            device.raw().allocate_command_buffers(&command_buffer_info).unwrap()[0];

        let buffer_begin_info = vk::CommandBufferBeginInfo {
            s_type: vk::StructureType::COMMAND_BUFFER_BEGIN_INFO,
            p_next: null(),
            flags: vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
            p_inheritance_info: null(),
        };
        device.raw().begin_command_buffer(command_buffer, &buffer_begin_info).unwrap();

        command_buffer
    }
}

pub fn submit_and_free_transient(
    device: &Device,
    command_buffer: vk::CommandBuffer,
) {
    unsafe {
        device.raw().end_command_buffer(command_buffer).unwrap();

        let submit_info = vk::SubmitInfo {
            s_type: vk::StructureType::SUBMIT_INFO,
            p_next: null(),
            wait_semaphore_count: 0,
            p_wait_semaphores: null(),
            p_wait_dst_stage_mask: null(),
            command_buffer_count: 1,
            p_command_buffers: &command_buffer,
            signal_semaphore_count: 0,
            p_signal_semaphores: null(),
        };

        device.raw().queue_submit(
            device.graphics_queue(), &[submit_info], vk::Fence::null()
        ).unwrap();

        // Wait till the upload has completed
        device.raw().queue_wait_idle(device.graphics_queue()).unwrap();

        device.raw().free_command_buffers(
            device.transient_command_pool(), &[command_buffer],
        );
    }
}

pub fn find_memorytype_index(
    memory_req: &vk::MemoryRequirements,
    memory_prop: &vk::PhysicalDeviceMemoryProperties,
    flags: vk::MemoryPropertyFlags,
) -> Option<u32> {
    // Try to find exactly matching memory flags
    let best_suitable_index =
        find_memorytype_index_f(memory_req, memory_prop, flags, |property_flags, flags| {
            property_flags == flags
        });
    if best_suitable_index.is_some() {
        return best_suitable_index;
    }

    // Otherwise find a memory type that just has the flags we want
    find_memorytype_index_f(memory_req, memory_prop, flags, |property_flags, flags| {
        property_flags & flags == flags
    })
}

fn find_memorytype_index_f<F: Fn(vk::MemoryPropertyFlags, vk::MemoryPropertyFlags) -> bool>(
    memory_req: &vk::MemoryRequirements,
    memory_prop: &vk::PhysicalDeviceMemoryProperties,
    flags: vk::MemoryPropertyFlags,
    f: F,
) -> Option<u32> {
    let mut memory_type_bits = memory_req.memory_type_bits;
    for (index, ref memory_type) in memory_prop.memory_types.iter().enumerate() {
        let type_in_requirements = memory_type_bits & 1 == 1;

        if type_in_requirements && f(memory_type.property_flags, flags) {
            return Some(index as u32)
        }
        
        memory_type_bits >>= 1;
    }
    None
}
