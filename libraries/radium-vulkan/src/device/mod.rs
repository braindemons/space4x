mod physical_device;

pub use self::{
    physical_device::{PhysicalDevice},
};

use {
    std::{ffi::{CStr}, ptr::{null}},

    log::{info, warn},
    ash::{
        extensions::{Surface},
        version::{InstanceV1_0, DeviceV1_0},
        vk,
        Device as VkDevice, Instance,
    },
};

pub struct Device {
    raw: VkDevice,

    physical_device: PhysicalDevice,
    graphics_queue: vk::Queue,

    /// Command pool optimized for resetting command buffers.
    resetting_command_pool: vk::CommandPool,
    /// Command pool optimized for transient command buffers.
    transient_command_pool: vk::CommandPool,
}

impl Device {
    pub unsafe fn new(
        instance: &Instance,
        surface_ext: &Surface, surface: vk::SurfaceKHR,
        physical_device: PhysicalDevice,
        required_device_extensions: &[*const i8],
        enable_validation: bool,
    ) -> Self {
        info!("Creating Vulkan Device");

        // Find a queue family for graphics
        let graphics_queue_family = physical_device
            .find_graphics_queue_family(surface_ext, surface).unwrap();

        let priority = 1.0;
        let graphics_queue_info = vk::DeviceQueueCreateInfo {
            s_type: vk::StructureType::DEVICE_QUEUE_CREATE_INFO,
            p_next: null(),
            flags: Default::default(),
            queue_family_index: graphics_queue_family,
            queue_count: 1,
            p_queue_priorities: &priority,
        };

        let physical_device_features = vk::PhysicalDeviceFeatures {
            sampler_anisotropy: vk::TRUE,
            texture_compression_bc: vk::TRUE,
            .. Default::default()
        };

        // If we've got validation enabled, we need the validation layers on the device as well
        let mut layers = Vec::new();
        if enable_validation {
            layers.push(
                CStr::from_bytes_with_nul(b"VK_LAYER_LUNARG_standard_validation\0")
                    .unwrap().as_ptr()
            );
        }

        // Create the device itself
        let device_info = vk::DeviceCreateInfo {
            s_type: vk::StructureType::DEVICE_CREATE_INFO,
            p_next: null(),
            flags: Default::default(),
            queue_create_info_count: 1,
            p_queue_create_infos: &graphics_queue_info,
            enabled_layer_count: layers.len() as u32,
            pp_enabled_layer_names: layers.as_ptr(),
            enabled_extension_count: required_device_extensions.len() as u32,
            pp_enabled_extension_names: required_device_extensions.as_ptr(),
            p_enabled_features: &physical_device_features,
        };
        let device = instance.create_device(physical_device.raw(), &device_info, None).unwrap();

        // Retrieve the graphics queue we previously requested from the device
        let graphics_queue = device.get_device_queue(graphics_queue_family, 0);

        let command_pool_info = vk::CommandPoolCreateInfo {
            s_type: vk::StructureType::COMMAND_POOL_CREATE_INFO,
            p_next: null(),
            flags: vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
            queue_family_index: graphics_queue_family,
        };
        let resetting_command_pool =
            device.create_command_pool(&command_pool_info, None).unwrap();

        let command_pool_info = vk::CommandPoolCreateInfo {
            s_type: vk::StructureType::COMMAND_POOL_CREATE_INFO,
            p_next: null(),
            flags: vk::CommandPoolCreateFlags::TRANSIENT,
            queue_family_index: graphics_queue_family,
        };
        let transient_command_pool =
            device.create_command_pool(&command_pool_info, None).unwrap();

        Self {
            raw: device,

            physical_device,
            graphics_queue,

            resetting_command_pool,
            transient_command_pool,
        }
    }

    pub fn raw(&self) -> &VkDevice {
        &self.raw
    }

    pub fn physical_device(&self) -> &PhysicalDevice {
        &self.physical_device
    }

    pub fn graphics_queue(&self) -> vk::Queue {
        self.graphics_queue
    }

    pub fn resetting_command_pool(&self) -> vk::CommandPool {
        self.resetting_command_pool
    }

    pub fn transient_command_pool(&self) -> vk::CommandPool {
        self.transient_command_pool
    }

    pub unsafe fn destroy(&self) {
        self.raw.destroy_command_pool(self.resetting_command_pool, None);
        self.raw.destroy_command_pool(self.transient_command_pool, None);

        self.raw.destroy_device(None);
    }
}

pub fn find_best(
    instance: &Instance, surface_ext: &Surface, surface: vk::SurfaceKHR,
    required_device_extensions: &[*const i8],
) -> PhysicalDevice {
    // Find the best physical device we can use
    info!("Picking Best Device");
    let mut found_device = None;
    let mut found_score = -1;
    for device in PhysicalDevice::all(instance) {
        if !device.has_extensions(&instance, &required_device_extensions) {
            warn!("Skipping Device {}: Required extensions not available", device.name());
            continue
        }

        if !device.can_render_to_surface(&surface_ext, surface) {
            warn!("Skipping Device {}: Can't render to surface", device.name());
            continue
        }

        if !device.has_required_features(&instance) {
            warn!("Skipping Device {}: Doesn't have required features", device.name());
            continue
        }

        let score = device.score();

        info!("Device {} scored {}", device.name(), score);

        if score > found_score {
            found_device = Some(device);
            found_score = score;
        }
    }

    let physical_device = found_device.expect(
        "No devices with adequate features found, your GPU may be too old or you need to\
        update your drivers"
    );
    info!("Picked Device {}", physical_device.name());

    physical_device
}
