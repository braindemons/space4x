use {
    std::{ffi::{CStr}},

    log::{warn},
    ash::{
        extensions::{Surface},
        version::{InstanceV1_0},
        vk,
        Instance,
    },
};

#[derive(Clone)]
pub struct PhysicalDevice {
    raw: vk::PhysicalDevice,
    properties: vk::PhysicalDeviceProperties,
    memory_properties: vk::PhysicalDeviceMemoryProperties,
    queue_family_properties: Vec<vk::QueueFamilyProperties>,
}

impl PhysicalDevice {
    pub fn from_raw(instance: &Instance, raw: vk::PhysicalDevice) -> Self {
        unsafe {
            let properties = instance.get_physical_device_properties(raw);
            let memory_properties = instance.get_physical_device_memory_properties(raw);
            let queue_family_properties =
                instance.get_physical_device_queue_family_properties(raw);

            Self {
                raw,
                properties,
                memory_properties,
                queue_family_properties,
            }
        }
    }

    pub fn all(instance: &Instance) -> Vec<Self> {
        unsafe {
            instance.enumerate_physical_devices().unwrap().into_iter()
                .map(|raw| PhysicalDevice::from_raw(instance, raw))
                .collect()
        }
    }

    pub fn raw(&self) -> vk::PhysicalDevice {
        self.raw
    }

    pub fn properties(&self) -> &vk::PhysicalDeviceProperties {
        &self.properties
    }

    pub fn memory_properties(&self) -> &vk::PhysicalDeviceMemoryProperties {
        &self.memory_properties
    }

    pub fn queue_family_properties(&self) -> &Vec<vk::QueueFamilyProperties> {
        &self.queue_family_properties
    }

    pub fn name(&self) -> String {
        unsafe {
            let name = CStr::from_ptr(&self.properties.device_name as *const i8);
            name.to_string_lossy().to_string()
        }
    }

    pub fn score(&self) -> i32 {
        match self.properties.device_type {
            // Discrete GPUs are preferable, integrated GPUs are not the greatest but fine
            vk::PhysicalDeviceType::DISCRETE_GPU => 100,
            vk::PhysicalDeviceType::INTEGRATED_GPU => 10,
            // CPUs will likely not run well at all but hey if you really want to
            vk::PhysicalDeviceType::CPU => 1,
            // I don't know enough about other types to score them well
            _ => 1,
        }
    }

    pub fn has_extensions(
        &self, instance: &Instance, required_device_extensions: &[*const i8],
    ) -> bool {
        let extensions = unsafe {
            instance.enumerate_device_extension_properties(self.raw).unwrap()
        };

        // Make sure the devices has all the extensions we absolutely need, otherwise we can't
        // show anything on screen
        for required_extension in required_device_extensions {
            let required_name = unsafe { CStr::from_ptr(*required_extension) };

            // Check if any of the device's extensions matches the one we need
            let mut has = false;
            for extension in &extensions {
                let name = unsafe { CStr::from_ptr(&extension.extension_name as *const i8) };
                if name == required_name {
                    has = true;
                    break;
                }
            }

            // If the extension wasn't found, this device can't be used
            if !has {
                return false
            }
        }

        true
    }

    pub fn can_render_to_surface(
        &self, surface_loader: &Surface, surface: vk::SurfaceKHR,
    ) -> bool {
        // Make sure this device supports the swap chain features we need
        let surface_present_modes = unsafe {
            surface_loader
                .get_physical_device_surface_present_modes_khr(self.raw, surface)
                .unwrap()
        };
        let surface_formats = unsafe {
            surface_loader
                .get_physical_device_surface_formats_khr(self.raw, surface)
                .unwrap()
        };
        if surface_formats.is_empty() ||
            surface_present_modes.is_empty() {
            return false
        }

        true
    }

    pub fn has_required_features(
        &self, instance: &Instance,
    ) -> bool {
        // Make sure this device has the features we need
        let features = unsafe { instance.get_physical_device_features(self.raw) };
        if features.sampler_anisotropy == 0 || features.texture_compression_bc == vk::FALSE {
            return false
        }

        true
    }

    pub fn find_present_mode(
        &self, surface_loader: &Surface, surface: vk::SurfaceKHR,
    ) -> vk::PresentModeKHR {
        let surface_present_modes = unsafe {
            surface_loader
                .get_physical_device_surface_present_modes_khr(self.raw, surface)
                .unwrap()
        };

        // We first want to check if our preferred mode is available
        for available_present_mode in surface_present_modes {
            if available_present_mode == vk::PresentModeKHR::MAILBOX {
                return available_present_mode;
            }
        }

        // Fall back to this if nothing else is available, this is standard wait-for-vblank VSync
        vk::PresentModeKHR::FIFO
    }

    pub fn find_surface_format(
        &self, surface_loader: &Surface, surface: vk::SurfaceKHR,
    ) -> vk::SurfaceFormatKHR {
        let surface_formats = unsafe {
            surface_loader
                .get_physical_device_surface_formats_khr(self.raw, surface)
                .unwrap()
        };

        // If anything's allowed, just go with what we want
        if surface_formats.len() == 1 &&
            surface_formats[0].format == vk::Format::UNDEFINED {
            return vk::SurfaceFormatKHR {
                format: vk::Format::B8G8R8A8_SRGB,
                color_space: vk::ColorSpaceKHR::SRGB_NONLINEAR,
            }
        }

        // If there are more restricted formats, find one that matches what we want
        for available_format in &surface_formats {
            if available_format.format == vk::Format::B8G8R8A8_SRGB &&
                available_format.color_space == vk::ColorSpaceKHR::SRGB_NONLINEAR {
                return *available_format
            }
        }

        // If what we want isn't available, fall back to the first available format
        let format = surface_formats[0];
        warn!(
            "Preferred swap surface format unavailable, using first available: {:?}",
            format.format,
        );
        format
    }

    pub fn find_graphics_queue_family(
        &self, surface_loader: &Surface, surface: vk::SurfaceKHR,
    ) -> Result<u32, ()> {
        for (index, family) in self.queue_family_properties().iter().enumerate() {
            if !family.queue_flags.contains(vk::QueueFlags::GRAPHICS) {
                continue
            }

            let supports_surface = unsafe {
                surface_loader.get_physical_device_surface_support_khr(
                    self.raw(),
                    index as u32,
                    surface,
                )
            };
            if !supports_surface {
                continue
            }

            return Ok(index as u32)
        }

        Err(())
    }
}
