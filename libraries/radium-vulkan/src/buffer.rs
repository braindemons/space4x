use {
    std::{ptr::{null}, mem},

    ash::{version::{DeviceV1_0}, util::{Align}, vk},

    crate::{
        device::{Device},
        memory::{
            find_memorytype_index, begin_transient, submit_and_free_transient,
        },
    },
};

pub struct DedicatedMemoryBuffer {
    pub buffer: vk::Buffer,
    pub memory: vk::DeviceMemory,
    pub size: u64,
}

impl DedicatedMemoryBuffer {
    pub unsafe fn new(
        device: &Device,
        size: u64, usage: vk::BufferUsageFlags, memory_flags: vk::MemoryPropertyFlags,
    ) -> Self {
        let buffer_info = vk::BufferCreateInfo {
            s_type: vk::StructureType::BUFFER_CREATE_INFO,
            p_next: null(),
            flags: Default::default(),
            size,
            usage,
            sharing_mode: vk::SharingMode::EXCLUSIVE,
            queue_family_index_count: 0,
            p_queue_family_indices: null(),
        };

        let buffer = device.raw().create_buffer(&buffer_info, None).unwrap();

        let buffer_memory_requirements = device.raw().get_buffer_memory_requirements(
            buffer,
        );

        let buffer_memory_type_index = find_memorytype_index(
            &buffer_memory_requirements,
            device.physical_device().memory_properties(),
            memory_flags,
        ).unwrap();
        let allocate_info = vk::MemoryAllocateInfo {
            s_type: vk::StructureType::MEMORY_ALLOCATE_INFO,
            p_next: null(),
            allocation_size: buffer_memory_requirements.size,
            memory_type_index: buffer_memory_type_index,
        };

        let memory = device.raw().allocate_memory(&allocate_info, None).unwrap();

        device.raw().bind_buffer_memory(buffer, memory, 0).unwrap();

        Self {
            buffer,
            memory,
            size,
        }
    }

    pub unsafe fn staging_with_data<T: Copy>(
        device: &Device,
        data: &[T],
    ) -> Self {
        let size = (mem::size_of::<T>() * data.len()) as u64;
        let buffer = Self::new(
            device, size,
            vk::BufferUsageFlags::TRANSFER_SRC,
            vk::MemoryPropertyFlags::HOST_VISIBLE | vk::MemoryPropertyFlags::HOST_COHERENT,
        );

        let ptr = device.raw().map_memory(
            buffer.memory, 0, size, Default::default(),
        ).unwrap();

        let mut align = Align::new(
            ptr,
            mem::align_of::<T>() as u64,
            size,
        );
        align.copy_from_slice(data);

        device.raw().unmap_memory(buffer.memory);

        buffer
    }

    pub unsafe fn destroy(&self, device: &Device) {
        let device = device.raw();
        device.destroy_buffer(self.buffer, None);
        device.free_memory(self.memory, None);
    }
}

pub fn submit_buffer_copies(
    device: &Device,
    src_dsts: &[(&DedicatedMemoryBuffer, &DedicatedMemoryBuffer)],
) {
    unsafe {
        let command_buffer = begin_transient(device);

        for (source, destination) in src_dsts {
            assert_eq!(source.size, destination.size);

            let copy_region = vk::BufferCopy {
                src_offset: 0,
                dst_offset: 0,
                size: source.size,
            };
            device.raw().cmd_copy_buffer(
                command_buffer, source.buffer, destination.buffer, &[copy_region]
            );
        }

        submit_and_free_transient(device, command_buffer);
    }
}
