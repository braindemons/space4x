use {
    ash::{
        util::{Align},
        version::{DeviceV1_0},
        vk,
    },
    log::{info},

    crate::{
        device::{Device},
        buffer::{DedicatedMemoryBuffer},
        Uniform,
    },
};

pub struct UniformsBuffer {
    pub buffer: DedicatedMemoryBuffer,
    pub uniforms_limit: usize,
    pub uniform_padded_size: u64,
    pub frameslot_size: u64,
}

impl UniformsBuffer {
    pub unsafe fn new(
        device: &Device,
        max_frames_in_flight: usize, uniforms_limit: usize,
    ) -> Self {
        info!("Creating Uniforms Buffer");

        // Create a dynamic uniform buffer, we fit all frames within the one buffer
        let alignment = device.physical_device().properties()
            .limits.min_uniform_buffer_offset_alignment;
        let size_unpadded = std::mem::size_of::<Uniform>() as u64;
        let padding = (alignment - size_unpadded % alignment) % alignment;

        let uniform_padded_size = size_unpadded + padding;
        let frameslot_size =
            uniforms_limit as u64 * uniform_padded_size;

        let size = frameslot_size * max_frames_in_flight as u64;
        let buffer = DedicatedMemoryBuffer::new(
            device, size,
            vk::BufferUsageFlags::UNIFORM_BUFFER,
            vk::MemoryPropertyFlags::HOST_VISIBLE | vk::MemoryPropertyFlags::HOST_COHERENT,
        );

        Self {
            buffer,
            uniforms_limit,
            uniform_padded_size,
            frameslot_size,
        }
    }

    pub unsafe fn copy_to_buffer(
        &self, device: &Device, uniforms: &[Uniform], uniforms_frame_offset: u32,
    ) {
        // TODO: Check if using a staging buffer and then copying to device local memory is
        // better for performance
        // TODO: Check if writing directly to the buffer is faster, instead of writing to an
        // intermediate array first
        let uniform_ptr = device.raw()
            .map_memory(
                self.buffer.memory,
                u64::from(uniforms_frame_offset),
                self.frameslot_size,
                vk::MemoryMapFlags::empty(),
            )
            .unwrap();
        let mut uniform_aligned_slice = Align::new(
            uniform_ptr,
            self.uniform_padded_size,
            self.frameslot_size,
        );
        uniform_aligned_slice.copy_from_slice(&uniforms);
        device.raw().unmap_memory(self.buffer.memory);
    }
    
    pub unsafe fn destroy(&self, device: &Device) {
        self.buffer.destroy(device);
    }
}
