use {
    std::{ptr::{null}},

    ash::{version::{DeviceV1_0}, vk},

    crate::{
        device::{Device},
        memory::{find_memorytype_index, begin_transient, submit_and_free_transient},
    },
};

pub unsafe fn create_image(
    device: &Device,
    width: u32, height: u32, format: vk::Format, usage: vk::ImageUsageFlags,
) -> (vk::Image, vk::DeviceMemory, vk::MemoryRequirements) {
    let image_info = vk::ImageCreateInfo {
        s_type: vk::StructureType::IMAGE_CREATE_INFO,
        p_next: null(),
        flags: Default::default(),
        image_type: vk::ImageType::TYPE_2D,
        extent: vk::Extent3D {
            width,
            height,
            depth: 1,
        },
        mip_levels: 1,
        array_layers: 1,
        format,
        tiling: vk::ImageTiling::OPTIMAL,
        initial_layout: vk::ImageLayout::UNDEFINED,
        usage,
        sharing_mode: vk::SharingMode::EXCLUSIVE,
        samples: vk::SampleCountFlags::TYPE_1,
        queue_family_index_count: 0,
        p_queue_family_indices: null(),
    };

    let image = device.raw().create_image(&image_info, None).unwrap();

    let memory_requirements = device.raw().get_image_memory_requirements(image);

    let alloc_info = vk::MemoryAllocateInfo {
        s_type: vk::StructureType::MEMORY_ALLOCATE_INFO,
        p_next: null(),
        allocation_size: memory_requirements.size,
        memory_type_index: find_memorytype_index(
            &memory_requirements,
            device.physical_device().memory_properties(),
            vk::MemoryPropertyFlags::DEVICE_LOCAL,
        ).unwrap(),
    };

    let memory = device.raw().allocate_memory(&alloc_info, None).unwrap();

    device.raw().bind_image_memory(image, memory, 0).unwrap();

    (image, memory, memory_requirements)
}

pub unsafe fn create_image_view(
    device: &Device, image: vk::Image, format: vk::Format, aspect_mask: vk::ImageAspectFlags,
) -> vk::ImageView {
    let view_info = vk::ImageViewCreateInfo {
        s_type: vk::StructureType::IMAGE_VIEW_CREATE_INFO,
        p_next: null(),
        flags: Default::default(),
        view_type: vk::ImageViewType::TYPE_2D,
        format,
        components: vk::ComponentMapping {
            r: vk::ComponentSwizzle::IDENTITY,
            g: vk::ComponentSwizzle::IDENTITY,
            b: vk::ComponentSwizzle::IDENTITY,
            a: vk::ComponentSwizzle::IDENTITY,
        },
        subresource_range: vk::ImageSubresourceRange {
            aspect_mask,
            base_mip_level: 0,
            level_count: 1,
            base_array_layer: 0,
            layer_count: 1,
        },
        image,
    };

    device.raw().create_image_view(&view_info, None).unwrap()
}

pub fn transition_image_layout(
    device: &Device,
    image: vk::Image, _format: vk::Format,
    old_layout: vk::ImageLayout, new_layout: vk::ImageLayout,
) {
    let command_buffer = begin_transient(device);

    let src_access_mask;
    let dst_access_mask;
    let source_stage;
    let destination_stage;

    // TODO: All these IFs probably need cleaning up

    let aspect_mask = if new_layout == vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL {
        vk::ImageAspectFlags::DEPTH
    } else {
        vk::ImageAspectFlags::COLOR
    };

    if old_layout == vk::ImageLayout::UNDEFINED {
        src_access_mask = Default::default();
        source_stage = vk::PipelineStageFlags::TOP_OF_PIPE;
    } else if old_layout == vk::ImageLayout::TRANSFER_DST_OPTIMAL {
        src_access_mask = vk::AccessFlags::TRANSFER_WRITE;
        source_stage = vk::PipelineStageFlags::TRANSFER;
    } else {
        panic!("Unsupported old layout");
    }

    if new_layout == vk::ImageLayout::TRANSFER_DST_OPTIMAL {
        dst_access_mask = vk::AccessFlags::TRANSFER_WRITE;
        destination_stage = vk::PipelineStageFlags::TRANSFER;
    } else if new_layout == vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL {
        dst_access_mask = vk::AccessFlags::SHADER_READ;
        destination_stage = vk::PipelineStageFlags::FRAGMENT_SHADER;
    } else if new_layout == vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL {
        dst_access_mask =
            vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_READ |
            vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_WRITE;
        destination_stage = vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS;
    } else {
        panic!("Unsupported new layout");
    }

    let barrier = vk::ImageMemoryBarrier {
        s_type: vk::StructureType::IMAGE_MEMORY_BARRIER,
        p_next: null(),
        old_layout,
        new_layout,

        src_queue_family_index: vk::QUEUE_FAMILY_IGNORED,
        dst_queue_family_index: vk::QUEUE_FAMILY_IGNORED,

        image,
        subresource_range: vk::ImageSubresourceRange {
            aspect_mask,
            base_mip_level: 0,
            level_count: 1,
            base_array_layer: 0,
            layer_count: 1,
        },

        src_access_mask,
        dst_access_mask,
    };
    unsafe {
        device.raw().cmd_pipeline_barrier(
            command_buffer,
            source_stage, destination_stage,
            Default::default(),
            &[],
            &[],
            &[barrier]
        );
    }

    submit_and_free_transient(device, command_buffer);
}

pub fn copy_buffer_to_image(
    device: &Device,
    buffer: vk::Buffer, image: vk::Image, width: u32, height: u32,
) {
    let command_buffer = begin_transient(device);

    let region = vk::BufferImageCopy {
        buffer_offset: 0,
        buffer_row_length: 0,
        buffer_image_height: 0,

        image_subresource: vk::ImageSubresourceLayers {
            aspect_mask: vk::ImageAspectFlags::COLOR,
            mip_level: 0,
            base_array_layer: 0,
            layer_count: 1,
        },

        image_offset: vk::Offset3D {
            x: 0, y: 0, z: 0,
        },
        image_extent: vk::Extent3D {
            width,
            height,
            depth: 1,
        },
    };

    unsafe {
        device.raw().cmd_copy_buffer_to_image(
            command_buffer,
            buffer,
            image,
            vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            &[region]
        );
    }

    submit_and_free_transient(device, command_buffer);
}
