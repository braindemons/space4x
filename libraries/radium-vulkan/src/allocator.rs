use {
    crate::device::{Device},
};

pub struct Allocator {
}

impl Allocator {
    pub unsafe fn new(_device: &Device) -> Self {
        Self {
        }
    }

    pub unsafe fn destroy(&self, _device: &Device) {
    }
}