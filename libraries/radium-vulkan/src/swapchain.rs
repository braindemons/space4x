use {
    std::{ptr::{null, null_mut}},

    log::{info},
    ash::{
        extensions::{Surface, Swapchain},
        version::{DeviceV1_0},
        vk, Instance,
    },

    crate::{
        device::{Device},
        image::{create_image, create_image_view, transition_image_layout},
    },
};

pub struct SwapchainManager {
    swapchain_ext: Swapchain,

    present_mode: vk::PresentModeKHR,
    surface_format: vk::SurfaceFormatKHR,

    current: Option<CurrentSwapchain>,
}

impl SwapchainManager {
    pub unsafe fn new(
        instance: &Instance, device: &Device,
        present_mode: vk::PresentModeKHR, surface_format: vk::SurfaceFormatKHR,
    ) -> Self {
        info!("Creating Vulkan Swapchain");
        
        let swapchain_ext = Swapchain::new(instance, device.raw());

        Self {
            swapchain_ext,

            present_mode,
            surface_format,

            current: None,
        }
    }

    pub fn invalidate(&mut self) {
        if let Some(current) = &mut self.current {
            current.valid = false;
        }
    }

    pub fn maintain(
        &mut self,
        device: &Device,
        surface_ext: &Surface, surface: vk::SurfaceKHR,
        render_pass: vk::RenderPass,
    ) {
        // Check for size invalidation before rendering just in case
        let should_recreate = self.current.as_mut()
            .map(|c| !c.valid)
            .unwrap_or(true);

        if should_recreate {
            let new_current = unsafe {
                CurrentSwapchain::new(
                    device,
                    surface_ext, surface,
                    render_pass,
                    &self.swapchain_ext,
                    self.present_mode, self.surface_format,
                    &self.current,
                )
            };
            
            if let Some(current) = &self.current {
                unsafe { current.destroy(device, &self.swapchain_ext); }
            }

            self.current = Some(new_current);
        }
    }

    /// Returns Some if aquiring was successful and rendering can continue.
    pub fn acquire(
        &mut self,
        image_available_semaphore: vk::Semaphore,
    ) -> Option<(vk::SwapchainKHR, vk::Extent2D, vk::Framebuffer, u32)> {
        let current = self.current.as_mut().unwrap();

        let result = unsafe {
            self.swapchain_ext.acquire_next_image_khr(
                current.swapchain,
                std::u64::MAX,
                image_available_semaphore,
                vk::Fence::null(),
            )
        };

        // If the swapchain is now invalid, we need to mark it as such and return nothing, it needs
        // to be re-created
        let (image_index, suboptimal) = match result {
            Ok(data) => data,
            Err(vk::Result::ERROR_OUT_OF_DATE_KHR) => {
                self.invalidate();
                return None
            },
            error => panic!(error),
        };

        if suboptimal {
            current.valid = false;
        }

        let framebuffer = current.framebuffers[image_index as usize];

        Some((current.swapchain, current.extent, framebuffer, image_index))
    }

    pub fn present(
        &mut self,
        graphics_queue: vk::Queue, swapchain: vk::SwapchainKHR,
        render_finished_semaphore: vk::Semaphore, image_index: u32,
    ) {
        let present_info = vk::PresentInfoKHR {
            s_type: vk::StructureType::PRESENT_INFO_KHR,
            p_next: null(),
            wait_semaphore_count: 1,
            p_wait_semaphores: &render_finished_semaphore,
            swapchain_count: 1,
            p_swapchains: &swapchain,
            p_image_indices: &image_index,
            p_results: null_mut(),
        };

        let result = unsafe {
            self.swapchain_ext.queue_present_khr(graphics_queue, &present_info)
        };

        match result {
            Ok(suboptimal) => {
                if suboptimal {
                    self.invalidate();
                }
            },
            Err(vk::Result::ERROR_OUT_OF_DATE_KHR) => {
                self.invalidate();
            },
            error => panic!(error),
        }
    }
    
    pub unsafe fn destroy(&self, device: &Device) {
        if let Some(current) = &self.current {
            current.destroy(device, &self.swapchain_ext);
        }
    }
}

struct CurrentSwapchain {
    valid: bool,
    swapchain: vk::SwapchainKHR,
    extent: vk::Extent2D,

    depth_image: vk::Image,
    depth_memory: vk::DeviceMemory,
    depth_view: vk::ImageView,

    image_views: Vec<vk::ImageView>,
    framebuffers: Vec<vk::Framebuffer>,
}

impl CurrentSwapchain {
    #[allow(clippy::too_many_arguments)]
    pub unsafe fn new(
        device: &Device,
        surface_ext: &Surface, surface: vk::SurfaceKHR,
        render_pass: vk::RenderPass,
        swapchain_ext: &Swapchain,
        present_mode: vk::PresentModeKHR, surface_format: vk::SurfaceFormatKHR,
        old: &Option<CurrentSwapchain>,
    ) -> Self {
        // We need a fresh capabilities when creating the swapchain to make sure it's up to date
        // for the current size of the target
        let capabilities = surface_ext
            .get_physical_device_surface_capabilities_khr(
                device.physical_device().raw(),
                surface,
            ).unwrap();

        let extent = find_extent(&capabilities);

        // Determine the amount of images we need for triple buffering
        let mut image_count = capabilities.min_image_count + 1;
        if capabilities.max_image_count > 0 {
            image_count = image_count.max(capabilities.max_image_count);
        }

        let swapchain_create_info = vk::SwapchainCreateInfoKHR {
            s_type: vk::StructureType::SWAPCHAIN_CREATE_INFO_KHR,
            p_next: null(),
            flags: Default::default(),
            surface,

            min_image_count: image_count,
            image_color_space: surface_format.color_space,
            image_format: surface_format.format,
            image_extent: extent,
            image_array_layers: 1,
            image_usage: vk::ImageUsageFlags::COLOR_ATTACHMENT,

            // Since we're looking for a queue that has both graphics and surface support, we can
            // assume these. If we want to support separate graphics and surface queues (some
            // platforms do need that) this needs to be changed.
            image_sharing_mode: vk::SharingMode::EXCLUSIVE,
            queue_family_index_count: 0,
            p_queue_family_indices: null(),

            pre_transform: capabilities.current_transform,
            composite_alpha: vk::CompositeAlphaFlagsKHR::OPAQUE,
            present_mode: present_mode,
            clipped: 1,

            old_swapchain: old.as_ref()
                .map(|o| o.swapchain)
                .unwrap_or_else(vk::SwapchainKHR::null),
        };

        let swapchain = swapchain_ext.create_swapchain_khr(&swapchain_create_info, None).unwrap();

        // Set up the depth buffer, we only need one because our semaphores ensure only one frame's
        // touching the depth buffer at a time.
        // If you want to change the format, this is currently hardcoded in pipeline as well
        let depth_format = vk::Format::D32_SFLOAT;
        let (depth_image, depth_memory, _) = create_image(
            device,
            extent.width, extent.height, depth_format,
            vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT,
        );
        let depth_view = create_image_view(
            device, depth_image, depth_format, vk::ImageAspectFlags::DEPTH,
        );
        transition_image_layout(
            device,
            depth_image, depth_format,
            vk::ImageLayout::UNDEFINED, vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        );

        let swapchain_images = swapchain_ext.get_swapchain_images_khr(swapchain).unwrap();
        let image_views: Vec<_> = swapchain_images.into_iter().map(|image| {
            create_image_view(
                device, image, surface_format.format, vk::ImageAspectFlags::COLOR,
            )
        }).collect();

        let framebuffers: Vec<_> = image_views.iter().map(|view| {
            let attachments = [*view, depth_view];
            let framebuffer_info = vk::FramebufferCreateInfo {
                s_type: vk::StructureType::FRAMEBUFFER_CREATE_INFO,
                p_next: null(),
                flags: Default::default(),
                render_pass,
                attachment_count: attachments.len() as u32,
                p_attachments: attachments.as_ptr(),
                width: extent.width,
                height: extent.height,
                layers: 1,
            };

            device.raw().create_framebuffer(&framebuffer_info, None).unwrap()
        }).collect();

        Self {
            valid: true,
            swapchain,
            extent,

            depth_image,
            depth_memory,
            depth_view,

            image_views,
            framebuffers,
        }
    }

    pub unsafe fn destroy(&self, device: &Device, swapchain_ext: &Swapchain) {
        let device = device.raw();
        
        device.device_wait_idle().unwrap();

        device.destroy_image_view(self.depth_view, None);
        device.destroy_image(self.depth_image, None);
        device.free_memory(self.depth_memory, None);

        for framebuffer in &self.framebuffers {
            device.destroy_framebuffer(*framebuffer, None);
        }

        for image_view in &self.image_views {
            device.destroy_image_view(*image_view, None);
        }

        swapchain_ext.destroy_swapchain_khr(self.swapchain, None);
    }
}

fn find_extent(
    capabilities: &vk::SurfaceCapabilitiesKHR,
) -> vk::Extent2D {
    // If we've been provided a size it SHOULD be, use that, don't do anything else
    if capabilities.current_extent.width != std::u32::MAX {
        return capabilities.current_extent
    }

    // We don't have a provided one, so we're just going to take some size we might want it to be
    // as a fallback, this might happen on some platforms. Right now it's hardcoded 1280 by 720,
    // the only reason for this is that we need some number at all.
    let extent = vk::Extent2D {
        width: 1280
            .max(capabilities.min_image_extent.width)
            .min(capabilities.max_image_extent.width),
        height: 720
            .max(capabilities.min_image_extent.height)
            .min(capabilities.max_image_extent.height),
    };

    // Just in case my assumptions were wrong
    assert!(extent.width != std::u32::MAX);
    assert!(extent.height != std::u32::MAX);

    extent
}
