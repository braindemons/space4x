mod timer;

use {
    specs::{
        World, Builder, DispatcherBuilder, Component, VecStorage, System, Join, ReadExpect,
        ReadStorage, WriteStorage,
    },
    nalgebra::{Vector3, Point3, UnitQuaternion},

    radium::{Window, Renderer},
    radium_assets::{AssetPackage},
    radium_3d::{
        assets::{MeshAsset, TextureAsset, PipelineAsset},
        components::{TransformComponent, MeshComponent, CameraComponent, ParentComponent},
        resources::{
            PipelineResources, MaterialData, MeshResources, MaterialResources, TextureResources,
        },
        systems::{UpdateMatricesSystem, ParentHierarchySystem},
    },

    crate::timer::{Timer},
};

pub fn run<W: Window, R: Renderer>(window: &mut W, renderer: &mut R) {
    let assets = AssetPackage::new("./assets/target/space4x");

    // Set up the game world
    let mut world = World::new();

    renderer.setup(&mut world.res);

    world.add_resource(DeltaTime { delta: 0.0 });

    let mut dispatcher = DispatcherBuilder::new()
        .with(RotateSystem, "rotate", &[])
        .with(ParentHierarchySystem::new(), "hierarchy", &[])
        .with(UpdateMatricesSystem::new(), "update_matrices", &["hierarchy", "rotate"])
        .build();
    dispatcher.setup(&mut world.res);

    initialize_world(&assets, &mut world);

    let mut timer = Timer::new();
    while !window.close_requested() {
        // Check and store the frame delta time
        world.write_resource::<DeltaTime>().delta = timer.next_delta();

        window.poll_events();

        // We don't have enough in the dispatcher yet where parallel dispatch makes sense
        //dispatcher.dispatch(&world.res);
        dispatcher.dispatch_seq(&world.res);
        dispatcher.dispatch_thread_local(&world.res);

        world.maintain();

        renderer.render(&world.res);
    }
}

fn initialize_world(assets: &AssetPackage, world: &mut World) {
    // Camera Entity
    world.create_entity()
        .with(TransformComponent {
            position: Point3::new(2.0, 3.0, 5.0),
            scale: Vector3::new(1.0, 1.0, 1.0),
            orientation: UnitQuaternion::look_at_rh(
                &Vector3::new(-2.0, -3.0, -5.0), &Vector3::new(0.0, 1.0, 0.0),
            ).inverse(),
        })
        .with(CameraComponent)
        .build();

    // Set up the Placeholder's resources
    let (
        pipeline, pipeline2, placeholder_mesh, cube_mesh,
        placeholder1_material, placeholder2_material,
    ) = {
        let mut pipelines = world.write_resource::<PipelineResources>();
        let mut meshes = world.write_resource::<MeshResources>();
        let mut textures = world.write_resource::<TextureResources>();
        let mut materials = world.write_resource::<MaterialResources>();

        let pipeline_asset = assets.load::<PipelineAsset>("pipeline");
        let pipeline = pipelines.create(pipeline_asset.data);

        let pipeline2_asset = assets.load::<PipelineAsset>("pipeline2");
        let pipeline2 = pipelines.create(pipeline2_asset.data);

        let placeholder_mesh_asset = assets.load::<MeshAsset>("placeholder-mesh");
        let placeholder_mesh = meshes.create(placeholder_mesh_asset.data);

        let cube_mesh_asset = assets.load::<MeshAsset>("cube-mesh");
        let cube_mesh = meshes.create(cube_mesh_asset.data);

        let placeholder1_texture_asset = assets.load::<TextureAsset>("placeholder1-texture");
        let placeholder1_texture = textures.create(placeholder1_texture_asset.data);
        let placeholder1_material = materials.create(MaterialData {
            base_color: placeholder1_texture,
        });

        let placeholder2_texture_asset = assets.load::<TextureAsset>("placeholder2-texture");
        let placeholder2_texture = textures.create(placeholder2_texture_asset.data);
        let placeholder2_material = materials.create(MaterialData {
            base_color: placeholder2_texture,
        });

        (
            pipeline, pipeline2, placeholder_mesh, cube_mesh,
            placeholder1_material, placeholder2_material,
        )
    };

    // Placeholder Entity
    let parent = world.create_entity()
        .with(TransformComponent::at(Point3::new(0.0, 0.0, 0.0)))
        .with(MeshComponent {
            mesh: placeholder_mesh,
            material: placeholder1_material,
            pipeline,
        })
        .with(RotateComponent { speed: 0.5 })
        .build();

    let child1 = world.create_entity()
        .with(TransformComponent::at(Point3::new(2.0, 0.0, 0.0))
            .with_scale_uniform(0.5)
        )
        .with(ParentComponent { parent })
        .with(MeshComponent {
            mesh: placeholder_mesh,
            material: placeholder2_material,
            pipeline,
        })
        .with(RotateComponent { speed: 0.5 })
        .build();

    let child2 = world.create_entity()
        .with(TransformComponent::at(Point3::new(2.0, 0.0, 0.0))
            .with_scale_uniform(0.5)
        )
        .with(ParentComponent { parent: child1 })
        .with(MeshComponent {
            mesh: cube_mesh,
            material: placeholder2_material,
            pipeline: pipeline2,
        })
        .with(RotateComponent { speed: 0.5 })
        .build();

    world.create_entity()
        .with(TransformComponent::at(Point3::new(2.0, 0.0, 0.0))
            .with_scale_uniform(0.5)
        )
        .with(ParentComponent { parent: child2 })
        .with(MeshComponent {
            mesh: placeholder_mesh,
            material: placeholder1_material,
            pipeline,
        })
        .with(RotateComponent { speed: 0.5 })
        .build();
}

struct DeltaTime {
    delta: f32,
}

struct RotateComponent {
    speed: f32,
}

impl Component for RotateComponent {
    type Storage = VecStorage<Self>;
}

struct RotateSystem;

impl<'a> System<'a> for RotateSystem {
    type SystemData = (
        ReadExpect<'a, DeltaTime>,
        ReadStorage<'a, RotateComponent>,
        WriteStorage<'a, TransformComponent>,
    );

    fn run(&mut self, (delta_time, rotates, mut transforms): Self::SystemData) {
        for (rotate, transform) in (&rotates, &mut transforms).join() {
            transform.orientation *= UnitQuaternion::from_euler_angles(
                0.0, rotate.speed * delta_time.delta, 0.0
            );
        }
    }
}