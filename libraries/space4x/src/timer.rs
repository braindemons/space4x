use {
    std::{
        time::{Instant},
    },

    log::{info},
};

pub struct Timer {
    last_frame_start: Instant,
    accumulator: f32,
    frames: usize,
}

impl Timer {
    pub fn new() -> Self {
        Timer {
            last_frame_start: Instant::now(),
            accumulator: 0.0,
            frames: 0,
        }
    }
    
    pub fn next_delta(&mut self) -> f32 {
        // Get the delta time since the last time this function has been called
        let frame_start = Instant::now();
        let frame_duration = frame_start.duration_since(self.last_frame_start);
        self.last_frame_start = frame_start;

        // Convert the duration to a f32 in seconds
        let delta = frame_duration.as_secs() as f32 +
            frame_duration.subsec_nanos() as f32 * 1e-9;

        // Log framerate
        self.accumulator += delta;
        self.frames += 1;
        if self.accumulator >= 10.0 {
            info!(
                "Average Delta {} ({} FPS)",
                10.0 / self.frames as f32,
                self.frames as f32 / 10.0,
            );
            self.accumulator -= 10.0;
            self.frames = 0;
        }

        delta
    }
}
