use {
    winit::{EventsLoop, WindowBuilder, Window as RealWindow, Event, WindowEvent},

    radium::{Window},
};

pub struct WinitWindow {
    events_loop: EventsLoop,
    window: RealWindow,
    events: Vec<Event>,

    close_requested: bool,
}

impl WinitWindow {
    pub fn new(title: &str) -> Self {
        let events_loop = EventsLoop::new();
        let window = WindowBuilder::new()
            .with_title(title)
            .with_dimensions((1280, 720).into())
            .with_min_dimensions((1, 1).into())
            .build(&events_loop)
            .unwrap();

        Self {
            events_loop,
            window,
            events: Vec::new(),

            close_requested: false,
        }
    }

    pub fn window(&self) -> &RealWindow {
        &self.window
    }
}

impl Window for WinitWindow {
    fn close_requested(&self) -> bool {
        self.close_requested
    }

    fn poll_events(&mut self) {
        let events = &mut self.events;
        self.events_loop.poll_events(|event| events.push(event));

        for event in events.drain(..) {
            if let Event::WindowEvent { event, .. } = event {
                if let WindowEvent::CloseRequested = event {
                    self.close_requested = true;
                }
            }
        }
    }
}