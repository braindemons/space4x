use {
    std::{
        path::{PathBuf},
        fs::{File},
        io::{BufReader},
    },

    serde::de::{DeserializeOwned},
    log::{info},
    
    crate::{Asset},
};

pub struct AssetPackage {
    root_path: PathBuf,
}

impl AssetPackage {
    pub fn new(root_path: impl Into<PathBuf>) -> AssetPackage {
        AssetPackage {
            root_path: root_path.into(),
        }
    }

    pub fn load<A: DeserializeOwned>(&self, name: &str) -> A {
        let mut full_path = self.root_path.clone();
        full_path.push(format!("{}.asset", name));

        info!("Loading Asset: {}", name);

        let file = File::open(full_path).unwrap();
        let reader = BufReader::new(file);
        let asset: Asset<A> = bincode::deserialize_from(reader).unwrap();

        // TODO: Verify asset class before loading the entire asset, this probably is going to
        // require a change to the asset format to be able to load it in segments of bincode.

        asset.data
    }
}