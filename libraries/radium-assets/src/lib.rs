pub mod build;

mod package;

pub use self::{
    package::{AssetPackage},
};

use {
    serde::{Deserialize, Serialize},
};

#[derive(Deserialize, Serialize)]
struct Asset<T> {
    class: String,
    data: T,
}