use {
    std::{
        collections::{HashMap},
        fs::{self, File},
        path::{PathBuf},
    },

    log::{info, error, debug},

    crate::{
        build::{
            package::{PackageSrc, AssetSrc},
            Error, AssetBuilder,
        },
        Asset,
    },
};

type BuilderFn = Fn(
    &HashMap<String, String>,
    &PathBuf,
    &PathBuf,
    &PathBuf,
) -> Result<(), Error>;

#[derive(Default)]
pub struct PackageBuilder {
    asset_builders: HashMap<String, Box<BuilderFn>>,
}

impl PackageBuilder {
    pub fn new() -> Self {
        PackageBuilder {
            asset_builders: HashMap::new(),
        }
    }

    pub fn add_builder<S: Into<String>, B: AssetBuilder>(&mut self, class: S, builder: B) {
        let class = class.into();

        let fn_class = class.clone();
        let func = move |
            data: &HashMap<String, String>,
            source_directory: &PathBuf,
            temp_directory: &PathBuf,
            target_file: &PathBuf,
        | {
            let data = builder.build(data, source_directory, temp_directory)?;
            let asset = Asset {
                class: fn_class.clone(),
                data,
            };

            let mut file = File::create(target_file).unwrap();
            bincode::serialize_into(&mut file, &asset).unwrap();

            Ok(())
        };

        self.asset_builders.insert(class, Box::new(func));
    }

    pub fn build(&self, package_src_path: &PathBuf) -> Result<(), Error> {
        // Open and parse the package source file
        let mut file = File::open(package_src_path)
            .map_err(|_| Error::PackageFileNotFound(package_src_path.display().to_string()))?;
        let source: PackageSrc = ron::de::from_reader(&mut file)
            .map_err(|error| Error::Parse { path: package_src_path.clone(), error, })?;
        
        let package_name = package_src_path.file_stem().unwrap()
            .to_string_lossy().to_string();

        let mut source_directory = package_src_path.clone();
        source_directory.pop();

        let mut target_directory = source_directory.clone();
        target_directory.push("target");
        target_directory.push(&package_name);
        ensure_exists(&target_directory)?;

        let mut temp_directory = source_directory.clone();
        temp_directory.push("target");
        temp_directory.push("temp");
        ensure_exists(&temp_directory)?;

        info!("Building Package: {}", package_name);

        for asset in &source.assets {
            self.build_asset(asset, &source_directory, &target_directory, &temp_directory)?;
        }

        Ok(())
    }

    fn build_asset(
        &self,
        asset: &AssetSrc,
        source_directory: &PathBuf, target_directory: &PathBuf,
        temp_directory: &PathBuf,
    ) -> Result<(), Error> {
        info!(" {} -> {}.asset", asset.class, asset.name);

        let mut target_file = target_directory.clone();
        target_file.push(format!("{}.asset", asset.name));

        // Find the builder for this asset class
        let builder = match self.asset_builders.get(&asset.class) {
            Some(builder) => builder,
            None => {
                error!("Unknown Asset Class {}", asset.class);
                return Ok(())
            },
        };

        let result = builder(
            &asset.data,
            source_directory,
            temp_directory,
            &target_file,
        );

        if let Err(error) = result {
            error!("Error while executing {}:\n{:?}", asset.class, error);
        }

        // Clean temp directory
        for entry in fs::read_dir(temp_directory)? {
            let entry = entry?;
            if entry.metadata()?.is_file() {
                fs::remove_file(entry.path())?;
            }
        }

        Ok(())
    }
}

fn ensure_exists(path: &PathBuf) -> Result<(), Error> {
    if !path.exists() {
        debug!("Creating directory \"{}\"", path.display());
        fs::create_dir_all(path)?;
    }

    Ok(())
}
