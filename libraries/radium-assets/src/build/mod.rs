mod builder;
mod package;

pub use {
    self::{
        builder::{PackageBuilder},
    },
};

use {
    std::{
        any::{Any},
        collections::{HashMap},
        path::{PathBuf},
        io,
    },

    serde::{Serialize},
};

#[derive(Debug)]
pub enum Error {
    PackageFileNotFound(String),

    // Generic Errors
    Io(io::Error),
    Parse { path: PathBuf, error: ron::de::Error },
    Generic(String),
}

impl From<io::Error> for Error {
    fn from(error: io::Error) -> Self {
        Error::Io(error)
    }
}

impl From<String> for Error {
    fn from(error: String) -> Self {
        Error::Generic(error)
    }
}

pub trait AssetBuilder: Any {
    type AssetData: Serialize;

    fn build(
        &self,
        data: &HashMap<String, String>,
        source_directory: &PathBuf,
        temp_directory: &PathBuf,
    ) -> Result<Self::AssetData, Error>;
}
