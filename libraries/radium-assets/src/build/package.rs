use {
    std::collections::{HashMap},
    serde_derive::{Serialize, Deserialize},
};

#[derive(Serialize, Deserialize, Debug)]
pub struct PackageSrc {
    pub assets: Vec<AssetSrc>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct AssetSrc {
    pub class: String,
    pub name: String,
    pub data: HashMap<String, String>,
}
