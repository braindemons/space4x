import bpy
import os

target = os.environ.get('AT_DAE_TARGET')
bpy.ops.wm.collada_export(filepath=target, use_texture_copies=False, apply_modifiers=True)
