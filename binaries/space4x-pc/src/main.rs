use {
    radium_vulkan::{VulkanRenderer},
    radium_winit::{WinitWindow},
};

fn main() {
    radium::setup_logging();

    let mut window = WinitWindow::new("Space4X");
    let mut renderer = VulkanRenderer::new(window.window(), "Space4X");

    space4x::run(&mut window, &mut renderer);
}
