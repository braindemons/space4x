use {
    log::{error},
    clap::{Arg, App},

    radium_assets::build::{PackageBuilder},
    radium_builders::{
        PipelineAssetBuilder, MeshAssetBuilder, TextureAssetBuilder,
    },
};

fn main() {
    radium::setup_logging();

    let matches = App::new("Space4X Package Tool")
        .version("0.1.0")
        .about("Builds an assets package.")
        .arg(Arg::with_name("verbose")
            .long("verbose")
            .short("v")
            .help("Enables verbose logging")
        )
        .arg(Arg::with_name("PACKAGE")
            .help("Sets the input package source file to build from")
            .required(true)
        )
        .get_matches();

    let package = matches.value_of("PACKAGE").unwrap();

    let mut builder = PackageBuilder::new();
    builder.add_builder("pipeline", PipelineAssetBuilder);
    builder.add_builder("mesh", MeshAssetBuilder {
        blender: "C:\\Users\\Layl\\Downloads\\blender-2.80-63150511a29-win64\\blender-2.80.0-git.63150511a29-windows64\\blender.exe".into()
    });
    builder.add_builder("texture", TextureAssetBuilder);
    let result = builder.build(&package.into());

    match result {
        Ok(()) => {},
        Err(error) => {
            error!("Failed to build package:\n{:?}", error);
        }
    }
}
