# Space 4X

## Getting the assets
Check out from `svn://<ADDRESS>:3690/repo/space4x-assets/trunk` into a directory `assets` in
the root of the project.

## Building & Running
```
./build-assets.ps1
cargo run --bin space4x
```

## License
Licensed under either of
 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT License (Expat) ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
